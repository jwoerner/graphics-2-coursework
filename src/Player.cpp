#include "../include/Player.h"

#include "../include/ParentedSpot.h"
#include "../include/PhysicsSystem.h"

#include "../include/glm/gtc/matrix_transform.hpp"
#include "../include/glm/gtx/vector_angle.hpp"

#include <iostream>
#include <sstream>

Player::Player(InputHandler &in) : input(in), maxSpeed(150.0f), accelIn(0.0f), accelFact(15.0f), steerIn(0.0f),
	steerFact(1.0f), steerLerp(0.5f), latFricFact(0.5f), backFricFact(0.05f) {

	state.pos = glm::vec3(0.0f, 0.0f, 4.0f);
	state.rot = glm::zero<glm::vec3>();
	state.vel = glm::zero<glm::vec3>();

	state.fwd = glm::vec3(-glm::sin(state.rot.x), 0.0f, glm::cos(state.rot.x));
	state.right = glm::vec3(-state.fwd.z, 0.0f, state.fwd.x);
	state.up = glm::cross(state.right, state.fwd);

	prevState = state;
}

void Player::init() {

	std::vector<tinyobj::shape_t> shape;
	std::stringstream cModel;
	cModel << "resource/models/player/" << mesh->getName() << "_c.obj";
	loadCollisionMesh(cModel.str());
	updateCollisionMesh(state);

	lights.push_back(new ParentedSpot(*this, glm::vec3(-0.7f, 1.7f, -2.9f), glm::vec3(0.0f, 0.0f, 0.0f), 60.0f, 1.0f));
	lights.push_back(new ParentedSpot(*this, glm::vec3(0.7f, 1.7f, -2.9f), glm::vec3(0.0f, 0.0f, 0.0f), 60.0f, 1.0f));
	lights.push_back(new ParentedSpot(*this, glm::vec3(-1.0f, 1.7f, 2.9f), glm::vec3(1.0f, 0.0f, 0.0f), glm::vec3(1.0f, 0.0f, 0.0f), glm::vec3(0.0f, 180.0f, 0.0f), 70.0f, 2.0f));
	lights.push_back(new ParentedSpot(*this, glm::vec3(1.0f, 1.7f, 2.9f), glm::vec3(1.0f, 0.0f, 0.0f), glm::vec3(1.0f, 0.0f, 0.0f), glm::vec3(0.0f, 180.0f, 0.0f), 70.0f, 2.0f));

	lights.at(0)->setEnabled(false);
	lights.at(1)->setEnabled(false);
	lights.at(2)->setEnabled(false);
	lights.at(3)->setEnabled(false);
}

void Player::resolveCollision(Collidable* other, std::vector<glm::vec3> &simplex) {

	glm::vec3 dir = (center - other->getCenter());
	dir = glm::normalize(dir);
	//std::printf("%5.3f, %5.3f, %5.3f\n", dir.x, dir.y, dir.z);
	dir *= 2.0f;
	state.vel.x = dir.x;
	state.vel.z = dir.z;
	//state.pos.x += dir.x;
	//state.pos.z += dir.z;
}

void Player::update(float dt, float timescale) {

	prevState = state;

	/* Accelerate the Player. */
	if (input.getKey(KEY_W)) {
		accelIn = -1.0f;
	} else if (input.getKey(KEY_S)) {
		lights.at(2)->setEnabled(true);
		lights.at(3)->setEnabled(true);
		accelIn = 1.0f;
	} else {
		lights.at(2)->setEnabled(false);
		lights.at(3)->setEnabled(false);
		accelIn = 0.0f;
	}
	/* Steer the player. */
	if (input.getKey(KEY_A)) {
		steerIn = 1.0f;
	} else if (input.getKey(KEY_D)) {
		steerIn = -1.0f;
	} else {
		steerIn = 0.0f;
	}
	if (input.isGamepadConnected()) {
		glm::vec2 lsPos = input.getLeftStickPos();
		/* If there is a gamepad, allow gamepad input. */
		if (input.getRightTrigger() >= input.getTriggerDeadZone())
			accelIn = -input.getRightTrigger(); // Forward acceleration.
		if (input.getLeftTrigger() > input.getTriggerDeadZone())
			accelIn = input.getLeftTrigger(); // Braking.		
		if (fabs(lsPos.x) >= input.getStickDeadZone())
			steerIn = -lsPos.x; // Steering.
	}
	/* Toggle the headlights with the keyboard. */
	if (input.getKey(KEY_H)) {
		unsigned long press = GetTickCount();
		if ((press - input.getLastTimePressed(KEY_H)) < KEY_PRESS_DELAY) {
			input.setKey(KEY_H, false);
			lights.at(0)->toggle();
			lights.at(1)->toggle();
		}
	}
	/* Toggle the headlights with the gamepad. */
	if (input.isGamepadConnected()
		&& input.getGamepadButton(PAD_Y)) {
			lights.at(0)->toggle();
			lights.at(1)->toggle();
	}
	/* An attempt at realistic handling. Works if you enjoy being stuck to (-1)-0 X dir. */
	//state.fwd = glm::vec3(glm::rotate(glm::vec4(prevState.fwd, 1.0f), steerAng, state.up));
	//float steerAmnt = dt * steerLerp;

	///* Recalculate all the direction vectors. */
	//state.fwd = glm::normalize((prevState.fwd + (state.fwd - prevState.fwd)) / steerAmnt);
	//std::cout << state.fwd.x << ", " << state.fwd.y << ", " << state.fwd.z << std::endl;
	//state.right = glm::vec3(-state.fwd.z, 0.0f, state.fwd.x);
	//state.up = glm::cross(state.right, state.fwd);

	///* Calculate some lateral friction to stop the car sliding. */
	//glm::vec3 latVel = state.right * glm::dot(state.vel, state.right);
	//glm::vec3 latFric = -latVel * latFricFact;
	///* Calculate friction to slow the car back down. */
	//glm::vec3 backFric = -state.vel * backFricFact;

	//state.vel += (latFric + backFric) * dt; // Update the velocity again.

	//glm::vec3 accelVec = state.fwd * accelIn * accelFact;
	///* Restrict the max speed of the Player. */
	//if (spd < maxSpeed) {
	//	state.vel += accelVec * dt;
	//}

	//state.pos += state.vel * dt;
	//state.rot.y = (glm::degrees(
	//	glm::acos(
	//		glm::dot(state.fwd, glm::vec3(0.0f, 0.0f, 1.0f))/
	//		(glm::length(state.fwd) * glm::length(glm::vec3(0.0f, 0.0f, 1.0f)))
	//	)
	//));

	if (glm::length(state.vel) < maxSpeed) {
		if (accelIn < 0.0f) {
			state.vel.x -= (glm::sin(-state.rot.y * (glm::pi<float>() / 180.0f)) * accelFact);
			state.vel.z -= (glm::cos(-state.rot.y * (glm::pi<float>() / 180.0f)) * accelFact);
		} else if (accelIn > 0.0f) {
			state.vel.x += (glm::sin(-state.rot.y * (glm::pi<float>() / 180.0f)) * accelFact);
			state.vel.z += (glm::cos(state.rot.y * (glm::pi<float>() / 180.0f)) * accelFact);
		}
	}

	state.vel *= timescale;

	float steerAng = steerIn * steerFact;
	steerAng = (maxXYZ.z - minXYZ.z)/((maxXYZ.z - minXYZ.z)/glm::sin(glm::degrees(steerAng)));
	steerAng *= accelIn;

	state.rotVel.y += steerAng;
	state.rotVel *= timescale;

	state.fwd = glm::rotate(glm::vec3(0.0f, 0.0f, -1.0f), state.rot.y, state.up);
	state.right = glm::vec3(-state.fwd.z, 0.0f, state.fwd.x);
	state.up = glm::cross(state.right, state.fwd);

	for (unsigned int i = 0; i < lights.size(); i++) {
		if (lights.at(i)->isEnabled()) {
			lights.at(i)->update(dt, timescale);
		}
	}
}

void Player::move(float dt) {

	state.pos += state.vel * dt;
	state.vel *= backFricFact * dt;

	if (glm::length(worldLimit) > 0.0f) {
		if (state.pos.x >= worldLimit.x)
			state.pos.x -= 256.0f;
		else if (state.pos.x <= -worldLimit.x)
			state.pos.x += 256.0f;

		if (state.pos.z >= worldLimit.y)
			state.pos.z -= 256.0f;
		else if (state.pos.z <= -worldLimit.y)
			state.pos.z += 256.0f;
	}

	state.rot += state.rotVel;
	state.rotVel *= latFricFact * dt;

	if (state.rot.y >= 180.0f) state.rot.y -= 360.0f;
	else if (state.rot.y <= -180.0f) state.rot.y += 360.0f;	
	
	//std::printf("X: %5.2f, Y: %5.2f, Z: %5.2f\n", state.pos.x, state.pos.y, state.pos.z);

	updateCollisionMesh(state);
}

void Player::render(const glm::mat4 &view, const glm::mat4 &proj, const std::vector<Light*> &lights, const Fog &fog, const float dt) {

	/* Predict the current position of the entity. */
	mvMat = glm::mat4(1.0f);
	/* Perform various transformations to the model-view matrix. */
	mvMat = glm::translate(mvMat, state.pos); // Move the entity around the world.
	mvMat = glm::rotate(mvMat, -state.rot.y, state.up);
	mesh->prepare(mvMat, view, proj, lights, fog);
	Drawable::getGLError("Ply transform upload");
	mesh->render();
	Drawable::getGLError("Ply render");
	//drawCollisionMesh(proj, view);
}

Player::~Player() {}