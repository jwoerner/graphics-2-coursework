#include "../include/TestCube.h"

#include "../include/glm/gtc/matrix_transform.hpp"

#include <sstream>

TestCube::TestCube() {

	state.pos = glm::vec3(5.0f, 0.0f, -10.0f);
	scale = glm::vec3(1.0f);
}

TestCube::TestCube(int n) {

	state.pos = glm::vec3(0.0f, -1.0f, 10.0f);
	scale = glm::vec3(100.0f, 1.0f, 100.0f);
}

void TestCube::init() {

	std::vector<tinyobj::shape_t> shape;
	std::stringstream cModel;
	cModel << "resource/models/" <<  mesh->getName() << "_c.obj";
	loadCollisionMesh(cModel.str());
	updateCollisionMesh(state);
}

void TestCube::update(float dt, float timescale) { }

void TestCube::resolveCollision(std::vector<glm::vec3> &shape, std::vector<glm::vec3> &simplex) {}

void TestCube::render(const glm::mat4 &view, const glm::mat4 &proj, const std::vector<Light*> &lights, const Fog &fog, const float dt) {

		/* Predict the current position of the entity. */
		mvMat = glm::mat4(1.0f);
		/* Perform various transformations to the model-view matrix. */
		mvMat = glm::translate(mvMat, state.pos); // Move the entity around the world.
		mvMat = glm::scale(mvMat, scale);
		mesh->prepare(mvMat, view, proj, lights, fog);
		mesh->render();
}

TestCube::~TestCube() {}