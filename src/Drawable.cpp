#include "../include/Drawable.h"

#include <iostream>
#include <sstream>

Drawable::Drawable() : vertCount(0), indexCount(0), created(false) {}

void Drawable::setup(Shader& shad) {
	
	shader = shad; // Attatch the shader to this object.
	GLint shdProg = shader.getHandle();
	glUseProgram(shdProg);

	/* Get the locations of required shader attributes. */
	GLint posAttrib = shader.getAttrib("v_pos");
	GLint norAttrib = shader.getAttrib("v_nor");
	GLint colAttrib = shader.getAttrib("v_ambient");
	GLint texAttrib = shader.getAttrib("v_tex");

	/* Generate the vertex array buffer. */
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	/* Generate the vertex buffer objects. */
	vbo = new GLuint[4];
	glGenBuffers(4, vbo);
	/* Create a VBO for vertex positions. */
	glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);
	glBufferData(GL_ARRAY_BUFFER, vertCount * sizeof(GLfloat), &verts.front(), GL_STATIC_DRAW);
	glEnableVertexAttribArray(posAttrib);
	glVertexAttribPointer(posAttrib, 3, GL_FLOAT, GL_FALSE, 0, 0);
	/* We always expect to see verticies. But we may not have normals, colours or UVs. */
	if (norms.size() > 0 && norAttrib > -1) {
		/* Create a VBO for vertex normals. */
		glBindBuffer(GL_ARRAY_BUFFER, vbo[1]);
		glBufferData(GL_ARRAY_BUFFER, vertCount * sizeof(GLfloat), &norms.front(), GL_STATIC_DRAW);
		glEnableVertexAttribArray(norAttrib);
		glVertexAttribPointer(norAttrib, 3, GL_FLOAT, GL_FALSE, 0, 0);
	}
	
	if (cols.size() > 0 && colAttrib > -1) {
		/* Create a VBO for vertex colour. */
		glBindBuffer(GL_ARRAY_BUFFER, vbo[2]);
		glBufferData(GL_ARRAY_BUFFER, vertCount * sizeof(GLfloat), &cols.front(), GL_STATIC_DRAW);
		glEnableVertexAttribArray(colAttrib);
		glVertexAttribPointer(colAttrib, 3, GL_FLOAT, GL_FALSE, 0, 0);
	}
	
	if (tex.size() > 0 && texAttrib > -1) {
		/* Create a VBO for vertex texture co-ordinates. */
		glBindBuffer(GL_ARRAY_BUFFER, vbo[3]);
		glBufferData(GL_ARRAY_BUFFER, (vertCount - (vertCount/3)) * sizeof(GLfloat), &tex.front(), GL_STATIC_DRAW);
		glEnableVertexAttribArray(texAttrib);
		glVertexAttribPointer(texAttrib, 2, GL_FLOAT, GL_FALSE, 0, 0);
	}
	
	/*  We also always expect to see a index.
		Generate the index array buffer. */
	glGenBuffers(1, &ibo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indexCount * sizeof(GLuint), &index.front(), GL_STATIC_DRAW);
	
	/* Unbind everything once we're done. */
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
	
	/* Clean up the arrays now we don't need them. */
	verts.clear();
	norms.clear();
	tex.clear();
	cols.clear();
	index.clear();

	created = true;
}

void Drawable::prepare(const glm::mat4 &mvMat, const glm::mat4 &view, 
	const glm::mat4 &proj, const std::vector<Light*> &lights, const Fog &fog) {

	GLuint shd = shader.getHandle();
	glUseProgram(shd);
	/* Get the position of a few common matricies. */
	GLint projLoc = shader.getUniform("projection");
	GLint viewLoc = shader.getUniform("view");
	GLint mvMatLoc = shader.getUniform("model");
	GLint normLoc = shader.getUniform("normal");
	GLint viewInvLoc = shader.getUniform("v_inv");

	glUniformMatrix4fv(projLoc, 1, GL_FALSE, &proj[0][0]);
	glUniformMatrix4fv(viewLoc, 1, GL_FALSE, &view[0][0]);
	glUniformMatrix4fv(mvMatLoc, 1, GL_FALSE, &mvMat[0][0]);
	
	if (normLoc != -1) {
		glm::mat3 inverse = glm::transpose(glm::inverse(glm::mat3(mvMat)));
		glUniformMatrix3fv(normLoc, 1, GL_FALSE, &inverse[0][0]);
	}

	if (viewInvLoc != -1) {
		glm::mat4 vI = glm::inverse(view);
		glUniformMatrix4fv(viewInvLoc, 1, GL_FALSE, &vI[0][0]);
	}
	Drawable::getGLError("matrix upload");

	for (unsigned int i = 0; i < lights.size(); i++) {

		std::stringstream l; l << "lights[" << i <<"]";
		/* Get the uniform locations. */
		GLint lightPos = shader.getUniform(l.str().append(".pos").c_str());
		GLint lightDiff = shader.getUniform(l.str().append(".diffuse").c_str());
		GLint lightSpec = shader.getUniform(l.str().append(".specular").c_str());
		GLint lightEnabled = shader.getUniform(l.str().append(".enabled").c_str());

		GLint spotDirPos = shader.getUniform(l.str().append(".spotDir").c_str());
		GLint spotSizePos = shader.getUniform(l.str().append(".spotSize").c_str());
		GLint spotCutoffPos = shader.getUniform(l.str().append(".spotCutoff").c_str());
		GLint spotMultPos = shader.getUniform(l.str().append(".spotMult").c_str());

		GLint lightCAtt = shader.getUniform(l.str().append(".constAttenuation").c_str());
		GLint lightLAtt = shader.getUniform(l.str().append(".linearAttenuation").c_str());
		GLint lightQAtt = shader.getUniform(l.str().append(".quadAttenuation").c_str());

		/* Upload all the light attributes. */
		glUniform4fv(lightPos, 1, &lights.at(i)->getPos()[0]);
		glUniform3fv(lightDiff, 1, &lights.at(i)->getCol()[0]);
		glUniform3fv(lightSpec, 1, &lights.at(i)->getSpec()[0]);
		glUniform1i(lightEnabled, lights.at(i)->isEnabled());

		glUniform3fv(spotDirPos, 1, &lights.at(i)->getSpotDir()[0]);
		glUniform1f(spotSizePos, lights.at(i)->getSpotSize());
		glUniform1f(spotCutoffPos, lights.at(i)->getSpotCutoff());
		glUniform1f(spotMultPos, lights.at(i)->getSpotMult());

		glUniform1f(lightCAtt, lights.at(i)->getConstantAttenuation());
		glUniform1f(lightLAtt, lights.at(i)->getLinearAttenuation());
		glUniform1f(lightQAtt, lights.at(i)->getQuadraticAttenuation());
	}
	Drawable::getGLError("lights upload");
	/* Upload the material. */
	mat.uploadMaterial(shader.getHandle());
	Drawable::getGLError("Material upload");
	/* Get positions for fog details. */
	GLint fogCol = shader.getUniform("f.colour");
	GLint fogStart = shader.getUniform("f.start");
	GLint fogEnd = shader.getUniform("f.end");
	GLint fogDensity = shader.getUniform("f.density");
	GLint fogType = shader.getUniform("f.type");
	/* Upload details for fog. */
	glUniform4fv(fogCol, 1, &fog.colour[0]);
	glUniform1f(fogStart, fog.startDist);
	glUniform1f(fogEnd, fog.endDist);
	glUniform1f(fogDensity, fog.density);
	glUniform1i(fogType, fog.type);
	Drawable::getGLError("fog upload");
}

void Drawable::render() {

	glBindVertexArray(vao);
	getGLError("Bind VAO");
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
	getGLError("Bind IBO");
	glDrawElements(GL_TRIANGLES, indexCount, GL_UNSIGNED_INT, 0);
	getGLError("Draw");
}

void Drawable::getGLError(char* custErr) {

	#ifdef _DEBUG
	GLenum error;
	if((error = glGetError()) != GL_NO_ERROR) {
		const GLubyte* errString = gluErrorString(error);
		std::fprintf(stderr, "(%s) OpenGL Error: %s\n", custErr, errString);
	}
	#endif
}

Drawable::~Drawable() {

	if (created) {
		/* Unbind the resources before we attempt to remove them. */
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		glBindVertexArray(0);

		/* Manually clean up the rest of the OpenGL mess. */
		glDeleteBuffers(1, &ibo);
		glDeleteBuffers(4, vbo);
		glDeleteVertexArrays(1, &vao);
	}
}