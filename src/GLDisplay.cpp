#include "../include/GLDisplay.h"

#include "../include/Drawable.h"

GLDisplay::GLDisplay() : hInstance(NULL), width(640), height(480), 
	vSync(false), fullScreen(false), className("OpenGL"), lockMouse(true) {}

GLDisplay::GLDisplay(unsigned int w, unsigned int h) : hInstance(NULL), width(w), height(h), 
	vSync(false), fullScreen(false), className("OpenGL"), lockMouse(true) {}

LRESULT CALLBACK GLDisplay::stWndProc(HWND hWnd, UINT msg, WPARAM wPar, LPARAM lPar) 
{
	GLDisplay* disp;

	if (msg == WM_NCCREATE) {
		/* If a GLDisplay is created, assign it a long so we can point to it. */
		SetWindowLong(hWnd, GWL_USERDATA, 
			(long)((LPCREATESTRUCT(lPar))->lpCreateParams));
	}
	/* Attempt to get the long that points to this GLDisplay if it exists. */
	disp = (GLDisplay*)GetWindowLong(hWnd, GWL_USERDATA);

	if (disp) {
		/* If this GLDisplay exists, call the real message handler. */
		return disp->wndProc(hWnd, msg, wPar, lPar);
	} else {
		/* Otherwise, we need to create the Windows Process first. */
		return DefWindowProc(hWnd, msg, wPar, lPar);
	}
}

LRESULT CALLBACK GLDisplay::wndProc(HWND hWnd, UINT msg, WPARAM wPar, LPARAM lPar) {

	/* Handle the messages passed to this. */
	switch (msg) {
	case WM_CLOSE:
		PostQuitMessage(0);
		return 0;
	case WM_SIZE:
		/* Get the new width and height stored in lParam. */
		resize(LOWORD(lPar), HIWORD(lPar)); // Run the resize function.
		return 0;
	case WM_SETFOCUS:
		/* Hide the mouse cursor when the window is in focus. */
		ShowCursor(FALSE);
		lockMouse = true;
		return 0;
	case WM_KILLFOCUS:
		/* Show the mouse cursor when the window leaves focus. */
		ShowCursor(TRUE);
		lockMouse = false;
		return 0;
	default:
		return DefWindowProc(hWnd, msg, wPar, lPar);
	}
}

bool GLDisplay::createGLWindow(char* title) {

	this->title = title;

	GLuint pixelFormat;
	WNDCLASS wc;
	DWORD dwExStyle;
	DWORD dwStyle;
	RECT windowRect;
	windowRect.left = (long)0;
	windowRect.right = (long)width;
	windowRect.top = (long)0;
	windowRect.bottom = (long)height;
	
	hInstance = GetModuleHandle(NULL);
	wc.style			= CS_HREDRAW | CS_VREDRAW | CS_OWNDC;	// The style of the window.
	wc.lpfnWndProc		= (WNDPROC)GLDisplay::stWndProc;	// The message handler.
	wc.cbClsExtra		= 0;	// How many extra bytes after this struct.
	wc.cbWndExtra		= 0;	// How many extra bytes after the window instance.
	wc.hInstance		= hInstance;	// The instance handle.
	wc.hIcon			= LoadIcon(NULL, IDI_WINLOGO);		// Which icon to use.
	wc.hCursor			= LoadCursor(NULL, IDC_ARROW);	// Which cursor to use.
	wc.hbrBackground	= NULL;	// The background type for this Window.
	wc.lpszMenuName		= NULL;	// The resource name for the class menu.
	wc.lpszClassName	= className;	// A name to reference this class by.

	if (!RegisterClass(&wc)) {
		MessageBox(NULL, "Failed to register the window!", "ERROR", MB_OK | MB_ICONEXCLAMATION);
		return false;
	}

	/* Calculate the center point of the screen. */
	HDC tmpDC = GetDC(NULL);
	int x = (GetDeviceCaps(tmpDC, HORZRES) - static_cast<int>(width)) / 2;
	int y = (GetDeviceCaps(tmpDC, VERTRES) - static_cast<int>(height)) / 2;
	ReleaseDC(NULL, tmpDC);

	if (fullScreen) {
		DEVMODE mode;
		memset(&mode, 0, sizeof(mode));
		mode.dmSize = sizeof(mode);
		mode.dmPelsWidth = (long)windowRect.right;
		mode.dmPelsHeight = (long)windowRect.bottom;
		mode.dmBitsPerPel = 32;
		mode.dmFields = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;

		if (ChangeDisplaySettings(&mode, CDS_FULLSCREEN) != DISP_CHANGE_SUCCESSFUL) {
			std::printf("Failed to enter fullscreen. Default to windowed!\n");
			fullScreen = false;
		}

		x = y = 0; // We don't want the screen to be centered any more.
	}

	if (fullScreen) {
		dwExStyle = WS_EX_APPWINDOW;
		dwStyle = WS_POPUP | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
		ShowCursor(FALSE);
	} else {
		dwExStyle = WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;
		dwStyle = WS_OVERLAPPEDWINDOW;
	}

	AdjustWindowRectEx(&windowRect, dwExStyle, false, dwExStyle);

	/* Attempt to create the window. */
	if (!(hWnd = CreateWindowEx(dwExStyle,
			className,
			title,
			dwStyle | WS_CLIPSIBLINGS | WS_CLIPCHILDREN,
			x, y, // Set the top left corner of the window to x, y.
			windowRect.right, // Set the width.
			windowRect.bottom, // Set the height.
			NULL,
			NULL,
			hInstance, // The handle for this process.
			(void*)this))) {
		/* If window creation fails at any point, destroy OGL. */
		killGLWindow();
		/* Then show this error message. */
		MessageBox(NULL, "Window failed to create!", "ERROR", 
			MB_OK | MB_ICONEXCLAMATION);
		return false;
	}

	static PIXELFORMATDESCRIPTOR pFD = {
		sizeof(PIXELFORMATDESCRIPTOR), // Size of this struct.
		1, // Version number. This should always be 1.
		PFD_DRAW_TO_WINDOW |	// These specify a few things, draws to window.
		PFD_SUPPORT_OPENGL |	// Supports OpenGL drawing.
		PFD_DOUBLEBUFFER,		// Double buffers the drawing.
		PFD_TYPE_RGBA, // Uses the RGBA colour system.
		24, // The number of bits in the colour buffer.
		0, 0, 0, 0, 0, 0, // Number of bits for RGBA bitplanes and shifts.
		0, 
		0, 
		0, 
		0, 0, 0, 0,
		24, // Number of bits for depth buffer. 
		8,	// Number of bits for stencil buffer.
		0,	// Auxiliary buffers (unsupported).
		PFD_MAIN_PLANE,
		0, 
		0, 0, 0
	};

	/* Try and get the Device Context. */
	if (!(hDC=GetDC(hWnd))) {
		killGLWindow();
		MessageBox(NULL, "Unable to create a device context!", "ERROR", 
			MB_OK | MB_ICONEXCLAMATION);
		return false;
	}
	/* Let windows pick the pixel format descriptor. */
	if (!(pixelFormat = ChoosePixelFormat(hDC, &pFD))) {
		killGLWindow();
		MessageBox(NULL, "Cannot find a matching PixelFormat!", "ERROR", 
			MB_OK | MB_ICONEXCLAMATION);
		return false;
	}
	/* Set the pixel format descriptor. */
	if (!SetPixelFormat(hDC, pixelFormat, &pFD)) {
		killGLWindow();
		MessageBox(NULL, "Cannot set the Pixelformat!", 
			"ERROR", MB_OK | MB_ICONEXCLAMATION);
		return false;
	}
	/* Createa a false rendering context for testing. */
	HGLRC tmpContext = wglCreateContext(hDC); // Create the rendering context.
	wglMakeCurrent(hDC, tmpContext); // Set it too.

	/* Initialise GLEW, display and error and quit if it fails. */
	GLenum err = glewInit();
	if (err != GLEW_OK) {
		killGLWindow();
		MessageBox(NULL, "Failed to initialise GLEW!", "GLEW Error", 
			MB_OK | MB_ICONEXCLAMATION);
		return false; // If GLEW fails to load, just give up.
	}

	int glAttribs[] = {
		WGL_CONTEXT_MAJOR_VERSION_ARB, OGL_VER_MAJ,	// Set required OpenGL major version.
		WGL_CONTEXT_MINOR_VERSION_ARB, OGL_VER_MIN, // Set required OpenGL minor version.
		WGL_CONTEXT_FLAGS_ARB, OGL_CON_TYPE,	// Set forward compatibility and debug options.
		0
	};

	/* Check that the PC can actually use the selected PFD properly. */
	if (wglewIsSupported("WGL_ARB_create_context") == 1) {	
		/* If it does support everything use that. */
		hRC = wglCreateContextAttribsARB(hDC, 0, glAttribs);
		wglMakeCurrent(NULL, NULL);
		wglDeleteContext(tmpContext); // remove the false context.
		wglMakeCurrent(hDC, hRC);
	} else {
		/* If it fails, use the old context type. */
		hRC = tmpContext;
	}

	if (wglewIsSupported("GL_EXT_texture_filter_anisotropic") || GLEW_EXT_texture_filter_anisotropic) {
		float val[1]; glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, val);
		std::printf("Anisotropic filtering supported! (max - %2.0fx)\n", val[0]);
	}

	/* Get the OpenGL version we are actually using. */
	int glVersion[2] = {0, 0};
	glGetIntegerv(GL_MAJOR_VERSION, &glVersion[0]);
	glGetIntegerv(GL_MINOR_VERSION, &glVersion[1]);
	std::fprintf(stdout, "OpenGL Version: %d.%d\n", glVersion[0], glVersion[1]);

	if (!hRC) return false;	// If the OpenGL context is invalid. Quit.

	ShowWindow(hWnd, SW_SHOW);
	SetForegroundWindow(hWnd);
	SetFocus(hWnd);

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glClearColor(0.4f, 0.6f, 0.9f, 0.0f);
	resize(width, height);

	return true; // Window created!
}

void GLDisplay::setVSync(bool enabled) {

	vSync = enabled;
	PFNWGLSWAPINTERVALEXTPROC wglSwapIntervalEXT = reinterpret_cast<PFNWGLSWAPINTERVALEXTPROC>(wglGetProcAddress("wglSwapIntervalEXT"));
	if (wglSwapIntervalEXT)
		wglSwapIntervalEXT(enabled ? 1 : 0);
}

void GLDisplay::resize(int w, int h) {

	/* Update the width and height of this display. */
	width = w;
	height = h;
	ChangeDisplaySettings(NULL, 0);
	/* Resize the viewport to match the change in size. */
	glViewport(0, 0, w, h);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
}

void GLDisplay::update() {

	SwapBuffers(hDC);
	Drawable::getGLError("swap");
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
	Drawable::getGLError("clear");
}

void GLDisplay::getMousePos(long *pos) const {

	POINT p;
	if (GetCursorPos(&p)) {
		if (ScreenToClient(hWnd, &p)) {
			pos[0] = p.x-width/2;
			pos[1] = p.y-height/2;
		}
	}
}

void GLDisplay::setMousePos(long x, long y) const {

	if (lockMouse) {
		POINT p;
		p.x = x; p.y = y;

		ClientToScreen(hWnd, &p);
		p.x += (width/2); p.y += (height/2);
		SetCursorPos(p.x, p.y);
	}
}

bool GLDisplay::makeFullscreen() {

	if (!fullScreen) {
		fullScreen = true;
		SetWindowLongPtr(hWnd, GWL_STYLE, WS_SYSMENU | WS_POPUP | 
			WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE);
		MoveWindow(hWnd, 0, 0, width, height, true);
		
		DEVMODE dm;
		dm.dmSize = sizeof(DEVMODE);
		dm.dmPelsHeight = height;
		dm.dmPelsWidth = width;
		dm.dmBitsPerPel = 24;
		dm.dmFields = DM_PELSWIDTH | DM_PELSHEIGHT | DM_BITSPERPEL;

		if (ChangeDisplaySettings(&dm, 0) != DISP_CHANGE_SUCCESSFUL) {
			std::fprintf(stderr, "Could not enter fullscreen!\n");
			makeWindowed();
			return false;
		}//*/
		resize(width, height);
	} else {
		std::fprintf(stdout, "Already fullscreen!\n");
	}

	return fullScreen;
}

bool GLDisplay::makeWindowed() {

	if (fullScreen) {
		fullScreen = false;
		RECT rect;
		rect.left = 0;
		rect.top = 0;
		rect.right = width;
		rect.bottom = height;

		SetWindowLongPtr(hWnd, GWL_STYLE, WS_CAPTION | WS_POPUPWINDOW | WS_VISIBLE);
		//AdjustWindowRect(&rect, WS_CAPTION | WS_POPUPWINDOW, false);
		MoveWindow(hWnd, 0, 0, rect.right - rect.left, rect.bottom - rect.top, true);

		if (ChangeDisplaySettings(0, 0) != DISP_CHANGE_SUCCESSFUL) {
			std::fprintf(stderr, "Could not return to windowed mode!\n");
			makeFullscreen();
			return false;
		}
		resize(width, height);
	} else {
		std::fprintf(stdout, "Already windowed!\n");
	}

	return !fullScreen;
}

float GLDisplay::time() {

	static __int64 start = 0;
	static __int64 frequency = 0;

	if (start==0)
	{
		QueryPerformanceCounter((LARGE_INTEGER*)&start);
		QueryPerformanceFrequency((LARGE_INTEGER*)&frequency);
		return 0.0f;
	}

	__int64 counter = 0;
	QueryPerformanceCounter((LARGE_INTEGER*)&counter);
	return (float) ((counter - start) / double(frequency));
}

void GLDisplay::killGLWindow() {

	/* If we are running in fullscreen, we need to exit it first. */
	if (fullScreen) {
		ChangeDisplaySettings(NULL, 0);
		ShowCursor(TRUE);
	}

	/* Attempt to remove the OGL context and window handles. */
	if (hRC) {
		if (!wglMakeCurrent(hDC, 0)) {
			/* If we can't remove a GL context, show this error. */
			MessageBox(NULL, "DC and RC context failed to release!", 
				"SHUTDOWN ERROR", MB_OK | MB_ICONINFORMATION);
		}
		if (!wglDeleteContext(hRC)) {
			/* If the rendering context fails to delete, show this error. */
			MessageBox(NULL, "Failed to release rendering context!",
				"SHUTDOWN ERROR", MB_OK | MB_ICONINFORMATION);
		}
		/* It all went as planned, remove the rendering context. */
		hRC = NULL;
	}

	if (hDC && !ReleaseDC(hWnd, hDC)) {
		/* If the device context exists, and we can't remove it, show this. */
		MessageBox(NULL, "Device Context failed to release!",
			"SHUTDOWN ERROR", MB_OK | MB_ICONINFORMATION);
	}
	if (hWnd && !DestroyWindow(hWnd)) {
		/* If the handle for this window can't be destroyed, show this. */
		MessageBox(NULL, "Failed to release hWnd!", "SHUTDOWN ERROR", 
			MB_OK | MB_ICONINFORMATION);
	}
	if (!UnregisterClass(className, hInstance)) {
		/* If the class fails to unregister, show this error. */
		MessageBox(NULL, "Could not unregister class!", 
			"SHUTDOWN ERROR", MB_OK | MB_ICONINFORMATION);
		hInstance = NULL;
	}
}

GLDisplay::~GLDisplay() {

	killGLWindow();
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE pInstance, 
	LPSTR lpCmdLine, int nCmdShow) {

	return main();
}