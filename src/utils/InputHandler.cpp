#include "../../include/utils/InputHandler.h"

#include <limits>

#ifdef max
	#undef max // Dammit windows.h.
#endif

InputHandler::InputHandler() : mouseSet(true), mouseX(0), mouseY(0), 
	stickDeadZone(0.25f), triggerDeadZone(0.1f), state(PLAYING) {

	/* Initialise the keys array properly. All keys default to false. */
	for (int i = 0; i < 256; i++) {
		keys[i] = false;
	}
}

bool InputHandler::isGamepadConnected() {

	ZeroMemory(&controller, sizeof(XINPUT_STATE));
	DWORD result = XInputGetState(0, &controller);
	
	return (result == ERROR_SUCCESS);
}

glm::vec2 InputHandler::getLeftStickPos() const {

	glm::vec2 result;
	result.x = (controller.Gamepad.sThumbLX / static_cast<float>(std::numeric_limits<short>::max()));
	result.y = (controller.Gamepad.sThumbLY / static_cast<float>(std::numeric_limits<short>::max()));

	result = glm::clamp(result, -1.0f, 1.0f);

	return result;
}

glm::vec2 InputHandler::getRightStickPos() const {

	glm::vec2 result;
	result.x = (controller.Gamepad.sThumbRX / static_cast<float>(std::numeric_limits<short>::max()));
	result.y = (controller.Gamepad.sThumbRY / static_cast<float>(std::numeric_limits<short>::max()));

	result = glm::clamp(result, -1.0f, 1.0f);

	return result;
}

float InputHandler::getLeftTrigger() const {
	
	float result = (controller.Gamepad.bLeftTrigger / static_cast<float>(std::numeric_limits<BYTE>::max()));
	result = glm::clamp(result, 0.0f, 1.0f);

	return result;
}

float InputHandler::getRightTrigger() const {

	float result = (controller.Gamepad.bRightTrigger / static_cast<float>(std::numeric_limits<BYTE>::max()));
	result = glm::clamp(result, 0.0f, 1.0f);

	return result;
}

bool InputHandler::getGamepadButton(unsigned short button) {

	return (button & controller.Gamepad.wButtons) > 0;
}