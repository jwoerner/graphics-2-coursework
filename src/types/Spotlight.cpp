#include "../../include/types/Spotlight.h"

Spotlight::Spotlight() {

	pos = glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);
	spec = glm::vec3(1.0f);
	spotDir = glm::vec3(0.0f, 0.0f, -1.0f);
	spotSize = 45.0f;
	spotCutoff = spotSize * 1.5f;
	spotMult = 1.0f;
}

Spotlight::Spotlight(glm::vec3 p, glm::vec3 dir, float size, 
	float multiplier) {

		pos = glm::vec4(p, 1.0f);
		spec = glm::vec3(1.0f);
		spotDir = dir;
		spotSize = glm::clamp(size, 1.0f, 90.0f);
		spotCutoff = spotSize * 1.5f;
		spotMult = glm::max(multiplier, 0.01f);
}

Spotlight::Spotlight(glm::vec3 p, glm::vec3 c, glm::vec3 s, 
	glm::vec3 dir, float size, float multiplier, float r) {
		
		pos = glm::vec4(p, 1.0f);
		col = c;
		spec = s;
		radius = r;
		spotDir = dir;
		spotSize = size;
		spotSize = glm::clamp(size, 1.0f, 90.0f);
		spotCutoff = spotSize * 1.5f;
		spotMult = glm::max(multiplier, 0.01f);
}