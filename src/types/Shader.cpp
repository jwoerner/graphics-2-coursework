#include "../../include/types/Shader.h"

#include <fstream>
#include <iostream>

Shader::Shader() {

	progName = "";
	progID = 0;
	vertShader = 0;
	fragShader = 0;
}

bool Shader::load(const std::string name, const char* vertFile, const char* fragFile) {

	GLint result = 0; // Store the compile result in this.

	progName = name; // Set the program name.
	progID = glCreateProgram(); // Create the program.

	/* Attempt to load the vertex shader. */
	vertShader = loadShader(vertFile, GL_VERTEX_SHADER);
	if (vertShader == 0) {
		/* If there was an error loading the source, give up. */
		std::cerr << "ERROR: Source could not be loaded! - " << vertFile << std::endl;
		return false;
	}
	/* Compile the source. */
	glCompileShader(vertShader);
	/* Check the shader compiled properly. */
	glGetShaderiv(vertShader, GL_COMPILE_STATUS, &result);
	if (!result) {
		std::cerr << "ERROR: Shader compile error! - " << vertFile << std::endl;
		std::cerr << "Shader log:  - " << getShaderLog(vertShader) << std::endl;
		return false; // If it failed, tell us why and give up.
	}

	/* Attempt to load the fragment shader (similar to vertex). */
	fragShader = loadShader(fragFile, GL_FRAGMENT_SHADER);
	if (fragShader == 0) {
		std::cerr << "ERROR: Source could not be loaded! - " << fragFile << std::endl;
		return false;
	}
	glCompileShader(fragShader);

	glGetShaderiv(fragShader, GL_COMPILE_STATUS, &result);
	if (!result) {
		std::cerr << "ERROR: Shader compile error! - " << fragFile << std::endl;
		std::cerr << "Shader log:  - " << getShaderLog(fragShader) << std::endl;
		return false;
	}

	/* Link the two shaders into the program. */
	glAttachShader(progID, vertShader);
	glAttachShader(progID, fragShader);
	glLinkProgram(progID);
	/* Check to see if the program linked properly. */
	glGetProgramiv(progID, GL_LINK_STATUS, &result);
	if (!result) {
		std::cerr << "ERROR: Program link error! - " << progName << std::endl;
		std::cerr << "Program log:  - " << getProgramLog(progID) << std::endl;
		return false; // Give up if it failed.
	}

	/* Everything completed, print to output. */
	std::cout << "Shader program loaded! - " << progName << std::endl;
	return true;
}

GLuint Shader::loadShader(const char* shaderFile, const GLenum type) {

	/* Check the the provided shader type is valid. */
	if (type != GL_VERTEX_SHADER && type != GL_FRAGMENT_SHADER) {
		std::cerr << "ERROR: Invalid shader type!" << std::endl;
		return 0; // Return zero if it's invalid.
	}

	std::string src;
	std::ifstream file(shaderFile, std::ios::binary);

	if (file.is_open()) {
		file.seekg(0, std::ios::end);
		/* Read the contents of the file into a string. */
		unsigned int fileSize = static_cast<unsigned int>(file.tellg());
		src.resize(fileSize);
		file.seekg(0, std::ios::beg);
		file.read(&src[0], fileSize);
	} else {
		std::cerr << "ERROR: Could not open shader source!" << std::endl;
		return 0;
	}
	/* Create the shader and attatch the source to it. */
	GLuint shader = glCreateShader(type);
	const GLchar* glSrc = reinterpret_cast<const GLchar*>(src.c_str());
	glShaderSource(shader, 1, &glSrc, 0);

	return shader;
}

std::string Shader::getShaderLog(const GLuint shader) const {

	GLsizei logSize = 0;
	std::string log;

	/* Get the log and store it in a string. */
	glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logSize);
	log.resize(logSize);
	glGetShaderInfoLog(shader, logSize, &logSize, &log[0]);

	return log;
}

std::string Shader::getProgramLog(const GLuint program) const {

	GLsizei logSize = 0;
	std::string log;

	/* Get the log and store it in a string. */
	glGetProgramiv(program, GL_INFO_LOG_LENGTH, &logSize);
	log.resize(logSize);
	glGetProgramInfoLog(program, logSize, &logSize, &log[0]);

	return log;
}

const GLint Shader::getAttrib(const GLchar* attrib) const {

	return glGetAttribLocation(progID, attrib);
}

const GLint Shader::getUniform(const GLchar* uni) const {

	return glGetUniformLocation(progID, uni);
}

Shader::~Shader() {

	glUseProgram(0);
	glDeleteShader(vertShader);
	glDeleteShader(fragShader);
	glDeleteProgram(progID);

	std::cout << "Shader deleted - " << progName << std::endl;
}