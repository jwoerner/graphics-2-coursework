#include "../../include/types/Plane.h"

Plane::Plane() : scale(glm::vec2(1.0f)) {

	state.pos = glm::zero<glm::vec3>();
	state.rot = glm::zero<glm::vec3>();
	state.fwd = glm::vec3(0.0f, 0.0f, -1.0f);
	state.right = glm::vec3(1.0f, 0.0f, 0.0f);
	state.up = glm::vec3(0.0f, 1.0f, 0.0f);

	mat.setAmbient(glm::vec3(1.0f));
	mat.setDiffuse(glm::vec3(1.0f));
	mat.setSpecular(glm::vec3(1.0f));
	mat.setShininess(1.0f);
}

Plane::Plane(glm::vec3 pos, glm::vec3 rot, glm::vec2 size) : scale(size) {

	state.pos = pos;
	state.rot = rot;	
	state.fwd = glm::vec3(-glm::sin(state.rot.x), 0.0f, glm::cos(state.rot.x));
	state.right = glm::vec3(-state.fwd.z, 0.0f, state.fwd.x);
	state.up = glm::cross(state.right, state.fwd);
}

void Plane::setup(Shader &shad) {

	vertCount = 12;
	indexCount = 6;

	verts.resize(vertCount, 0.0f);
	verts[0] = 0.0f;	verts[1] = 0.0f;	verts[2] = 0.0f;
	verts[3] = scale.x;	verts[4] = 0.0f;	verts[5] = 0.0f;
	verts[6] = 0.0f;	verts[7] = 0.0f;	verts[8] = scale.y;
	verts[9] = scale.x;	verts[10] = 0.0f;	verts[11] = scale.y;

	norms.resize(vertCount, 0.0f);
	norms[0] = state.up.x; norms[1] = state.up.y;  norms[2] = state.up.z;
	norms[3] = state.up.x; norms[4] = state.up.y;  norms[5] = state.up.z;
	norms[6] = state.up.x; norms[7] = state.up.y;  norms[8] = state.up.z;
	norms[9] = state.up.x; norms[10] = state.up.y; norms[11] = state.up.z;

	tex.resize(vertCount - (vertCount / 3), 0.0f);
	tex[0] = 0.0f;				tex[1] = 0.0f;
	tex[2] = scale.s * 0.5f;	tex[3] = 0.0f;
	tex[4] = 0.0f;				tex[5] = scale.t * 0.5f;
	tex[6] = scale.s * 0.5f;	tex[7] = scale.t * 0.5f;

	index.resize(indexCount, 0);
	index[0] = 0; index[1] = 1; index[2] = 2;
	index[3] = 2; index[4] = 3; index[5] = 1;

	Drawable::setup(shad);
}

void Plane::render(const glm::mat4 &view, const glm::mat4 &proj, 
	const std::vector<Light*> &lights, const Fog &fog, const float dt) {

		glm::mat4 mvMat(1.0f);
		mvMat = glm::rotate(mvMat, state.rot.x, glm::vec3(1.0f, 0.0f, 0.0f));
		mvMat = glm::rotate(mvMat, state.rot.y, glm::vec3(0.0f, 1.0f, 0.0f));
		mvMat = glm::rotate(mvMat, state.rot.z, glm::vec3(0.0f, 0.0f, 1.0f));
		mvMat = glm::translate(mvMat, state.pos);

		Drawable::prepare(mvMat, view, proj, lights, fog);
		Drawable::getGLError("Plane prepare");
		Drawable::render();
		Drawable::getGLError("Plane render");
}