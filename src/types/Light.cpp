#include "../../include/types/Light.h"

Light::Light() : col(glm::vec4(1.0f)), spotSize(350.0f), spotMult(1.0f), 
	spotCutoff(360.0f), radius(1.0f), enabled(true), constantAttenuation(0.5f), 
	linearAttenuation(0.05f), quadraticAttenuation(0.005f) {}

Light::Light(glm::vec4 p) : pos(p), col(glm::vec3(1.0f)), spotSize(350.0f), 
	spotCutoff(360.0f), spotMult(1.0f), radius(1.0f), enabled(true), 
	constantAttenuation(0.5f), linearAttenuation(0.05f), 
	quadraticAttenuation(0.005f) {}

Light::Light(glm::vec4 p, glm::vec3 c, glm::vec3 s, float r) : pos(p), col(c), 
	spec(s),  spotSize(350.0f), spotMult(1.0f), spotCutoff(360.0f), radius(r), 
	enabled(true), constantAttenuation(0.5f), linearAttenuation(0.05f), 
	quadraticAttenuation(0.005f) {}