#include "../../include/types/Mesh.h"

#include "../../include/glm/glm.hpp"

#include <fstream>
#include <string>
#include <sstream>

Mesh::Mesh() : flat(false) {}

Mesh::Mesh(bool meshType) : flat(meshType) {}

void Mesh::setMesh(tinyobj::shape_t &mesh) {

	name	= mesh.name;
	verts	= std::vector<GLfloat>(mesh.mesh.positions);
	norms	= std::vector<GLfloat>(mesh.mesh.normals);
	tex		= std::vector<GLfloat>(mesh.mesh.texcoords);
	index	= std::vector<GLuint>(mesh.mesh.indices);
	
	vertCount = verts.size();
	indexCount = index.size();
}

void Mesh::calcNorms() {

	assert(verts.size() != 0);

	/* If there are no normals, or flat shading is specified. */
	if (norms.size() == 0) {
		/* If the file opens, process the lines. */
		std::vector<glm::vec3> tVerts, tNorms;
		std::vector<glm::vec2> tUVs;
		norms.clear();
		for (unsigned int i = 0; i < verts.size(); i+=3) {
			tVerts.push_back(glm::vec3(verts.at(i), verts.at(i+1), verts.at(i+2)));
		}
		/* Generate normals for each vertex. */
		if (flat) {
			verts.clear();
			/* Flat shading, all verticies in a face share normals. */
			for (unsigned int i = 0; i < index.size(); i++) {
				for (int n = 0; n < 3; n++) {
					verts.push_back(tVerts.at(index.at(i)).x);
					verts.push_back(tVerts.at(index.at(i)).y);
					verts.push_back(tVerts.at(index.at(i)).z);
				}
				if (tex.size() > 0) {
					tex.push_back(tUVs.at(index.at(i)).s);
					tex.push_back(tUVs.at(index.at(i)).t);
				}
				/* Calculate the normal for each vertex. */
				if ((i % 3) == 2) {
					GLuint ia = index.at(i-2);
					GLuint ib = index.at(i-1);
					GLuint ic = index.at(i);
					glm::vec3 ac = (tVerts.at(ic) - tVerts.at(ia));
					glm::vec3 ab = (tVerts.at(ib) - tVerts.at(ia));
					glm::vec3 nor = glm::normalize(glm::cross(ac, ab));
					/* Store the normal. */
					for (int n = 0; n < 3; n++) {
						tNorms.push_back(nor);
					}
				}
			}
		} else {
			/* Smooth shading, vertex normals interpolate. */
			std::vector<GLfloat> nor_coeff;
			tNorms.resize(tVerts.size(), glm::vec3(0.0f));
			nor_coeff.resize(tVerts.size(), 0);
			for (unsigned int i = 0; i < index.size(); i+=3) {
				/* Calculate the normal as usual. */
				GLuint ia = index.at(i);
				GLuint ib = index.at(i+1);
				GLuint ic = index.at(i+2);

				/* Skip this iteration if any of the points match up. We can't cross that. */
				if (ia != ib || ia != ic || ic != ib) {
					continue;
				}

				glm::vec3 ac = (tVerts.at(ic) - tVerts.at(ia));
				glm::vec3 ab = (tVerts.at(ib) - tVerts.at(ia));
				glm::vec3 nor = glm::normalize(glm::cross(ab, ac));

				int v[3]; v[0] = ia; v[1] = ib; v[2] = ic;
				for (int n = 0; n < 3; n++) {
					GLuint curV = v[n];
					nor_coeff[curV]++;
					if (nor_coeff.at(curV) == 1) {
						/* If the normal has already been averaged, just use it. */
						tNorms[curV] = nor;
					} else {
						/* Otherwise, average out the normal. */
						tNorms[curV].x = tNorms[curV].x * (1.0f - 1.0f/nor_coeff[curV]) + nor.x * 1.0f/nor_coeff[curV];
						tNorms[curV].y = tNorms[curV].y * (1.0f - 1.0f/nor_coeff[curV]) + nor.y * 1.0f/nor_coeff[curV];
						tNorms[curV].z = tNorms[curV].z * (1.0f - 1.0f/nor_coeff[curV]) + nor.z * 1.0f/nor_coeff[curV];
						tNorms[curV] = glm::normalize(tNorms[curV]);
					}
				}
			}
		}

		for (unsigned int i = 0; i < tNorms.size(); i++) {
			norms.push_back(tNorms.at(i).x);
			norms.push_back(tNorms.at(i).y);
			norms.push_back(tNorms.at(i).z);
		}
	}

	vertCount = verts.size();
	indexCount = index.size();
}

void Mesh::prepare(const glm::mat4 &mvMat, const glm::mat4 &view, const glm::mat4 &proj, const std::vector<Light*> &lights, const Fog &fog) {
		
	/* Prepare the core parts of the model. */
	Drawable::prepare(mvMat, view, proj, lights, fog);
	mat.uploadTextures(shader.getHandle());
	Drawable::getGLError("Texture upload");
}

Mesh::~Mesh() {}