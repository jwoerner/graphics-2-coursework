#include "..\..\include\types\QuadTree.h"

QuadTree::QuadTree() : level(-1), xPos(0), xSize(0), yPos(0), ySize(0) {}

QuadTree::QuadTree(int level, float x, float y, float w, float h) :
	level(level), xPos(x), xSize(w), yPos(y), ySize(h)
{
	nodes[0] = new QuadTree;
	nodes[1] = new QuadTree;
	nodes[2] = new QuadTree;
	nodes[3] = new QuadTree;
}

void QuadTree::split() {
		
	float newW = xSize / 2;
	float newH = ySize / 2;

	nodes[0] = new QuadTree(level + 1, xPos + newW, yPos, newW, newH);
	nodes[1] = new QuadTree(level + 1, xPos, yPos, newW, newH);
	nodes[2] = new QuadTree(level + 1, xPos, yPos + newH, newW, newH);
	nodes[3] = new QuadTree(level + 1, xPos + newW, yPos + newH, newW, newH);
}

int QuadTree::getIndex(const glm::vec3 &min, const glm::vec3 &max) const {

	int index = -1;
	glm::vec3 center = glm::vec3((xPos + xSize) / 2.0f, 0.0f, (yPos + ySize) / 2.0f);

	/* Check if the object is totally in the top or bottom quadrants. */
	bool isTop = (min.z < center.z && max.z < center.z);
	bool isBottom = (min.z > center.z);

	if (min.x < center.x && max.x < center.x) {
		if (isTop) index = 1;
		else if (isBottom) index = 2;
	} else if (min.x > center.x) {
		if (isTop) index = 0;
		else if (isBottom) index = 3;
	}

	return index;
}

void QuadTree::insert(Collidable* ent) {

	if (ent == NULL) return;

	if (nodes[0]->level != -1) {
		int index = getIndex(ent->getMinXYZ(), ent->getMaxXYZ());

		if (index != -1) {
			nodes[index]->insert(ent);
			return;
		}
	}

	items.push_back(ent);

	if (items.size() > QUADTREE_MAX_ITEMS && level < QUADTREE_MAX_LEVELS) {
		if (nodes[0]->level == -1)
			split();

		unsigned int i = 0;
		while (i < items.size()) {
			Collidable* c = items.at(i);
			int index = getIndex(c->getMinXYZ(), c->getMaxXYZ());

			if (index != -1) {
				nodes[index]->insert(items.back());
				items.pop_back();
			} else 
				i++;
		}
	}
}

void QuadTree::retrieve(std::vector<Collidable*> &objects, 
	const glm::vec3 &min, const glm::vec3 &max) const {

	int index = getIndex(min, max);

	if (index != -1 && nodes[0]->level != -1) {
		/* Add all the items in the sub-quadrants. */
		nodes[index]->retrieve(objects, min, max);
	}
	/* Add all the items in this quadrant. */
	objects.insert(objects.end(), items.begin(), items.end());
}