#include "../../include/types/Material.h"

#include "../../include/Drawable.h"

Material::Material() 
	: ambient(glm::vec3(1.0f)), diffuse(glm::vec3(1.0f)), specular(glm::vec3(1.0f)), shininess(0.5),
	ambientTex(0), diffuseTex(0), specularTex(0), normalTex(0) {}

Material::Material(glm::vec3 a, glm::vec3 d, glm::vec3 s, float s2)
	: ambient(a), diffuse(d), specular(s), shininess(s2),
	ambientTex(0), diffuseTex(0), specularTex(0), normalTex(0) {}

void Material::uploadMaterial(GLuint shader) {

	/* Find locations for the material properies. */
	GLint ambLoc = glGetUniformLocation(shader, "m.ambient");
	GLint diffLoc = glGetUniformLocation(shader, "m.diffuse");
	GLint specLoc = glGetUniformLocation(shader, "m.spec");
	GLint shineLoc = glGetUniformLocation(shader, "m.shine");
	Drawable::getGLError("Mat location");
	/* Upload the material details. */
	glUniform4fv(ambLoc, 1, &ambient[0]);
	glUniform4fv(diffLoc, 1, &diffuse[0]);
	glUniform3fv(specLoc, 1, &specular[0]);
	glUniform1f(shineLoc, shininess);
}

void Material::uploadTextures(GLuint shader) {

	/* Find locations for the textures. */
	GLint ambLoc = glGetUniformLocation(shader, "ambientTex");
	glUniform1i(ambLoc, 0);
	GLint difLoc = glGetUniformLocation(shader, "diffuseTex");
	glUniform1i(difLoc, 2);
	GLint spcLoc = glGetUniformLocation(shader, "specularTex");
	glUniform1i(spcLoc, 4);
	GLint norLoc = glGetUniformLocation(shader, "normalTex");
	glUniform1i(norLoc, 6);
	Drawable::getGLError("Tex location");

	/* Upload ambient texture. */
	if (ambLoc > -1) {
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, ambientTex);
		Drawable::getGLError("Amb Tex Up");
	}
	/* Upload diffuse texture. */
	if (difLoc > -1) {
		glActiveTexture(GL_TEXTURE0 + 2);
		glBindTexture(GL_TEXTURE_2D, diffuseTex);
		Drawable::getGLError("Dif Tex Up");
	}
	/* Upload specular map. */
	if (spcLoc > -1) {
		glActiveTexture(GL_TEXTURE0 + 4);
		glBindTexture(GL_TEXTURE_2D, specularTex);
		Drawable::getGLError("Spc Tex Up");
	}
	/* Upload normal map. */
	if (norLoc > -1) {
		glActiveTexture(GL_TEXTURE0 + 6);
		glBindTexture(GL_TEXTURE_2D, normalTex);
		Drawable::getGLError("Nor Tex Up");
	}
}