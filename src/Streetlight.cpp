#include "../include/Streetlight.h"

#include "../include/ParentedSpot.h"

#include "../include/glm/gtx/vector_angle.hpp"
#include "../include/glm/gtc/matrix_transform.hpp"

Streetlight::Streetlight() : lightPos(glm::vec3(-1.26f, 6.37f, 0.0f)),
	lightCol(glm::vec3(1.0f)), time(0.0f), broken(false)
{ 
	state.fwd = glm::vec3(glm::sin(state.rot.x), -glm::cos(state.rot.x), 0.0f);
	state.right = glm::vec3(-state.fwd.z, 0.0f, state.fwd.x);
	state.up = glm::cross(state.right, state.fwd);
}

Streetlight::Streetlight(glm::vec3 pos, glm::vec3 rot) 
	: lightPos(glm::vec3(-1.26f, 6.37f, 0.0f)), lightCol(glm::vec3(1.0f)), 
		time(0.0f), broken(false) {

	state.pos = pos;
	state.rot = rot;

	state.fwd = glm::vec3(glm::sin(state.rot.x), -glm::cos(state.rot.x), 0.0f);
	state.right = glm::vec3(-state.fwd.z, 0.0f, state.fwd.x);
	state.up = glm::cross(state.right, state.fwd);

	lightPos = glm::rotate(lightPos, rot.y, glm::vec3(0.0f, 1.0f, 0.0f));
}

Streetlight::Streetlight(glm::vec3 pos, glm::vec3 rot, glm::vec3 col) 
	: lightPos(glm::vec3(-1.26f, 6.37f, 0.0f)), lightCol(col), time(0.0f), 
		broken(false) {

	state.pos = pos;
	state.rot = rot;

	state.fwd = glm::vec3(glm::sin(state.rot.x), -glm::cos(state.rot.x), 0.0f);
	state.right = glm::vec3(-state.fwd.z, 0.0f, state.fwd.x);
	state.up = glm::cross(state.right, state.fwd);

	lightPos = glm::rotate(lightPos, rot.y, glm::vec3(0.0f, 1.0f, 0.0f));
}

void Streetlight::init() {

	lights.push_back(new ParentedSpot(*this, lightPos, lightCol, lightCol, glm::vec3(0.0f, 0.0f, 0.0f), 80.0f, 1.0f));
	lights.at(0)->setEnabled(false); // Light starts turned off.

	std::vector<tinyobj::shape_t> shape;
	std::stringstream cModel;
	cModel << "resource/models/" <<  mesh->getName() << "_c.obj";
	loadCollisionMesh(cModel.str());
	updateCollisionMesh(state);
}

void Streetlight::update(float dt, float timescale) {

	/* If there is still a light on this Streetlamp, update stuff. */
	if (!lights.empty() && !broken) {
		time += 5.0f * timescale * dt;
		if (time >= 360.0f) { time -= 360.0f; lights.at(0)->toggle(); }

		State tmpState = state;	
		state.up = glm::vec3(0.0f, 1.0f, 0.0f);
		lights.at(0)->update(dt, timescale);
		state = tmpState;
	}
	
	updateCollisionMesh(state); // Always update this.
}

void Streetlight::resolveCollision(Collidable* other, 
	std::vector<glm::vec3> &simplex) {

		glm::vec3 dir = (center - other->getCenter());
		dir = glm::normalize(dir);
		if (fabs(state.rot.x) < 70.0f && fabs(state.rot.x) < 70.0f) {
			state.rot += dir * 2.0f;
			/* If the new rotation is over 40 degrees on either axis. */
			if (fabs(state.rot.x) > 40.0f 
				|| fabs(state.rot.z) > 40.0f) {
					/* Turn off light. No point sending a broken light to the shader. */
					lights.at(0)->setEnabled(false);
					broken = true;
			}
		}
}

void Streetlight::move(float dt) {

	/* Stop it from falling too far over. */
	if (state.rot.x > 70.0f) state.rot.x = 70.0f;
	if (state.rot.x < -70.0f) state.rot.x = -70.0f;
	if (state.rot.z > 70.0f) state.rot.x = 70.0f;
	if (state.rot.z < -70.0f) state.rot.x = -70.0f;
}

void Streetlight::render(const glm::mat4 &view, const glm::mat4 &proj, 
	const std::vector<Light*> &lights, const Fog &fog, const float dt) {

	/* Predict the current position of the entity. */
	glm::mat4 mvMat = glm::mat4(1.0f);
	mvMat = glm::translate(mvMat, state.pos); // Move the entity around the world.
	/* Perform various transformations to the model-view matrix. */
	mvMat = glm::rotate(mvMat, state.rot.x, glm::vec3(1.0f, 0.0f, 0.0f));
	mvMat = glm::rotate(mvMat, state.rot.y, glm::vec3(0.0f, 1.0f, 0.0f));
	mvMat = glm::rotate(mvMat, state.rot.z, glm::vec3(0.0f, 0.0f, 1.0f));
	mesh->prepare(mvMat, view, proj, lights, fog);
	mesh->render();
}