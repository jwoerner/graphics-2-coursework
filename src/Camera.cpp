#include "../include/Camera.h"

#include "../include/glm/glm.hpp"
#include "../include/glm/gtc/constants.hpp"
#include "../include/glm/gtc/matrix_transform.hpp"
#include "../include/glm/gtx/vector_angle.hpp"

#include <iostream>

#define CAM_X_LIMIT glm::pi<float>() * 2
#define CAM_Y_LIMIT glm::half_pi<float>() - 0.2f

Camera::Camera(InputHandler &in) : target(NULL), orbit(true), speed(0.1f), 
	ev(in), sensitivity(0.01f), camMode(0) {

	offset = glm::vec3(-0.6f, 2.5f, 1.5f);
	/* Set up the directional vectors. */
	state.fwd = glm::normalize(glm::vec3(glm::sin(state.rot.x), 0.0f, glm::cos(state.rot.x)));
	state.right = glm::vec3(-state.fwd.z, 0.0f, state.fwd.x);
	state.up = glm::cross(state.right, state.fwd);

	lastMouse[0] = lastMouse[1] = 0L;
}

Camera::Camera(Camera &other) : ev(other.ev) {

	state = other.state;
	aimPos = other.aimPos;
	target = other.target;
	orbit = other.orbit;
	fov = other.fov;
	width = other.width;
	height = other.height;
	sensitivity = other.sensitivity;
	camMode = other.camMode;

	lastMouse[0] = other.lastMouse[0];
	lastMouse[1] = other.lastMouse[1];
}

void Camera::setFOV(unsigned int w, unsigned int h, float f) {

	width = w;
	height = h;

	float aspect = static_cast<float>(height)/static_cast<float>(width);
	/* Convert from horizontal to vertical. */
	f = glm::radians(f);
	f = 2.0f * atanf(tanf(f/2.0f) * aspect);
	fov = glm::degrees(f);
}

float Camera::getHFOV() {

	float aspect = static_cast<float>(width)/static_cast<float>(height);
	/* Convert from vertical to horizontal. */
	float f = glm::radians(fov);
	f = 2.0f * atanf(tanf(f/2.0f) * aspect);
	return glm::degrees(f);
}

void Camera::update(float timescale) {

	/* If C or gamepad back are pressed, change camera mode. */
	if (ev.getKey(KEY_C)) {
		lastPressed = GetTickCount();
		if ((lastPressed - ev.getLastTimePressed(KEY_C)) > KEY_PRESS_DELAY)
			changeMode();
	}
	if (ev.isGamepadConnected() && ev.getGamepadButton(PAD_BACK))
		changeMode();

	/* Adjust the camera rotation based on mouse movement. */
	if (ev.isMouseLocked() // The mouse needs to be locked to the game.
		&& (ev.getKey(KEY_LALT) // Make sure the player has left alt down.
			|| camMode == 2) // Or that they are in free-look mode.
		) {
		/* Update the rotation based on the change in mouse position. */
		state.rot.x -= static_cast<float>(ev.getMouseX()) * sensitivity;
		state.rot.y -= static_cast<float>(ev.getMouseY()) * sensitivity;
	}
	if (ev.isGamepadConnected()) {
		/* If there is a gamepad connected, use the right stick to look around. */
		glm::vec2 stickPos = ev.getRightStickPos();
		if (fabs(stickPos.x) >= ev.getStickDeadZone()) // Ignore small motions.
			state.rot.x -= stickPos.x * (sensitivity * 2);
		if (fabs(stickPos.y) >= ev.getStickDeadZone())
			state.rot.y -= stickPos.y * (sensitivity * 2);
	}

	/* Stop the camera exceeding certain rotations. */
	if (state.rot.x < -(glm::pi<float>())) {
		state.rot.x += CAM_X_LIMIT;
	} else if (state.rot.x > glm::pi<float>()) {
		state.rot.x -= CAM_X_LIMIT;
	}
	/* Lock the Y axis rotation from flipping the camera. */
	if (orbit) {
		if (state.rot.y < 0.2f)
			state.rot.y = 0.2f; // Stop the camera from going under the car.
		if (state.rot.y > CAM_Y_LIMIT)
			state.rot.y = CAM_Y_LIMIT;
	} else {
		if (state.rot.y < -(CAM_Y_LIMIT))
			state.rot.y = -(CAM_Y_LIMIT);
		if (state.rot.y > CAM_Y_LIMIT)
			state.rot.y = CAM_Y_LIMIT;
	}

	/* Update directional vectors. */
	state.fwd = glm::normalize(glm::vec3(glm::sin(state.rot.x), 0.0f, glm::cos(state.rot.x)));
	state.right = glm::vec3(-state.fwd.z, 0.0f, state.fwd.x);
	state.up = glm::cross(state.right, state.fwd);
	
	/* Update the position based on the camera type and if it has a target. */
	if (target != NULL && camMode != 2) {
		aimPos = target->getState().pos;
		state.pos = target->getState().pos;
	} else {
		if (ev.getKey(KEY_LEFT) // If the left arrow key is pressed.
			|| (ev.isGamepadConnected() // Or a gamepad is connected a dpad-left is pressed.
				&& ev.getGamepadButton(PAD_DPAD_LEFT))) {
			/* Double the movement speed if the left shift or left bumper button are held down. */
			state.pos -= state.right * (ev.getKey(KEY_LSHFT) || (ev.isGamepadConnected() && ev.getGamepadButton(PAD_LB)) ? speed * 2.0f : speed);
		} else if (ev.getKey(KEY_RIGHT)
			|| (ev.isGamepadConnected()
				&& ev.getGamepadButton(PAD_DPAD_RIGHT))) {
			state.pos += state.right * (ev.getKey(KEY_LSHFT) || (ev.isGamepadConnected() && ev.getGamepadButton(PAD_LB)) ? speed * 2.0f : speed);
		}
		if (ev.getKey(KEY_UP)
			|| (ev.isGamepadConnected()
				&& ev.getGamepadButton(PAD_DPAD_UP))) {
					state.pos += state.fwd * (ev.getKey(KEY_LSHFT) || (ev.isGamepadConnected() && ev.getGamepadButton(PAD_LB)) ? speed * 2.0f : speed);
		} else if (ev.getKey(KEY_DOWN)
			|| (ev.isGamepadConnected()
				&& ev.getGamepadButton(PAD_DPAD_DOWN))) {
			state.pos -= state.fwd * (ev.getKey(KEY_LSHFT) || (ev.isGamepadConnected() && ev.getGamepadButton(PAD_LB)) ? speed * 2.0f : speed);
		}
	}
	
	ev.setMouseX(0); ev.setMouseY(0); ev.setMousePosSet(false);
}

const glm::vec3 Camera::getPos() const {

	if (!orbit)
		return state.pos + offset;
	else
		return state.pos;
}

const glm::mat4 Camera::getView() {

	if (camMode == 0) {
		/* Orbit the camera around a target. */
		float radius = 10.0f;
		state.pos = offset;
		/* Move the camera around to ensure the skybox is in the right place. */
		state.pos.x = aimPos.x + radius * glm::cos(state.rot.x) * glm::cos(state.rot.y);
		state.pos.y = aimPos.y + radius * glm::sin(state.rot.y);
		state.pos.z = aimPos.z + radius * glm::sin(state.rot.x) * glm::cos(state.rot.y);
		/* Calculate the Camera rotation for an orbiting Camera. */
		return glm::lookAt(
			state.pos, 
			aimPos,
			state.up
		);
	} else {
		/* Calculate the Camera rotation for a first person Camera. */
		aimPos.x = glm::sin(state.rot.x) * glm::cos(state.rot.y);
		aimPos.y = glm::sin(state.rot.y);
		aimPos.z = glm::cos(state.rot.x) * glm::cos(state.rot.y);
		/* Rotate the offset so it follows the car properly. */
		if (camMode == 1) {
			offset.x *= glm::cos(state.rot.x) * glm::cos(state.rot.y);
			offset.z *= glm::sin(state.rot.x) * glm::cos(state.rot.y);
		}
		aimPos += (state.pos + offset);
		/* Calculate the view matrix. */
		return glm::lookAt(
			state.pos + offset,  
			aimPos,
			state.up
		);
	}
}

void Camera::changeMode() {

	/* Change camera mode based on the currently selected mode. */
	switch(camMode) {
	case 0:
		/* Switch to fixed first person. */
		orbit = false;
		camMode = 1;
		break;
	case 1:
		/* Switch to free first person. */
		camMode = 2;
		break;
	case 2:
		/* Switch to orbiting. */
		orbit = true;
		camMode = 0;
		break;
	}

	ev.setKey(KEY_C, false); // Stop the key press.
}

Camera::~Camera() {

	if (target != NULL)
		target = NULL; // Clear the target pointer if it's set.
}

Camera& Camera::operator=(const Camera& other) {

	if(this != &other) {
		state = other.state;
		aimPos = other.aimPos;
		target = other.target;
		orbit = other.orbit;
		fov = other.fov;
		width = other.width;
		height = other.height;
		camMode = other.camMode;

		lastMouse[0] = other.lastMouse[0];
		lastMouse[1] = other.lastMouse[1];
	}

	return *this;
}