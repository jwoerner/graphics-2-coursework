#include "../include/Camera.h"
#include "../include/GLDisplay.h"
#include "../include/Scene.h"
#include "../include/types/Shader.h"
#include "../include/utils/DebugConsole.h"

#include "../include/glm/glm.hpp"
#include "../include/glm/gtc/matrix_transform.hpp"

#define WIN32_LEAN_AND_MEAN // Remove bits of Win32 I don't care about.
#include <Windows.h>

/*
 Returns the left/ right handedness of certain special keys.
 */
WPARAM handleKeyHandedness(WPARAM wPar, LPARAM lPar) {

	WPARAM newPar;
	unsigned int scanCode = (lPar & 0x00ff0000) >> 16;
	int extendedKey = (lPar & 0x01000000) != 0;

	switch(wPar) {
	case VK_SHIFT: // Shift key handedness.
		newPar = MapVirtualKey(scanCode, MAPVK_VSC_TO_VK_EX);
		break;
	case VK_CONTROL: // Ctrl key handedness.
		newPar = extendedKey ? VK_RCONTROL : VK_LCONTROL;
		break;
	case VK_MENU: // Alt key handedness.
		newPar = extendedKey ? VK_RMENU : VK_LMENU;
		break;
	default: // No special key.
		newPar = wPar;
		break;
	}

	return newPar;
}

/*
 Main program.
 */
int main() {

	#ifdef _DEBUG
	/* If we are debugging the game, open the console. */
	DebugConsole con;
	con.open(); // Open the console first
	#endif
	GLDisplay* display = new GLDisplay(1024, 768);
	display->setVSync(false);
	display->setFullscreen(false);
	bool done = display->createGLWindow("Car Simulator") ? false : true; // Check if the display was created.

	if (!done) {
		/* If there is a display, we need to run the game. */
		MSG msg;
		float timeScale = 1.0f;
		InputHandler ev;
		long mousePos[2] = {0, 0};
		Camera cam(ev);
		Scene world(&ev, cam);
		ev.setMouseX(0); ev.setMouseY(0); ev.setMousePosSet(false);

		float t = 0.0f;
		const float dt = 0.01f;

		double nextTick = 0.0f;
		double accum = 0.0;

		world.init();
		cam.setFOV(display->getWidth(), display->getHeight(), 90.0f);
		std::fprintf(stdout, "FOV - V:%3.2f, H:%3.2f degrees\n", cam.getVFOV(), cam.getHFOV());

		glm::mat4 perspective = glm::perspective(cam.getVFOV(), display->getAspect(), 0.1f, 200.0f);
		world.setProjection(perspective);

		while (!done) {
			double curTime = GLDisplay::time();
			double frame = curTime - nextTick;
			if (frame <= 0.0)
				continue;
			if (frame > 0.25)
				frame = 0.25;

			nextTick = curTime;
			accum += frame;

			/* Check the Windows message pump. */
			while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
				/* If a message is sent, handle it appropriately. */
				switch(msg.message) {
				case WM_QUIT:
					/* If the message is "quit", end the loop. */
					done = true;
					break;
				case WM_KEYDOWN:	// Catch key releases.
				case WM_SYSKEYDOWN:
					if (msg.wParam == VK_ESCAPE) {
						/* Quite the game if ESC is pressed. */
						done = true;
					}
					msg.wParam = handleKeyHandedness(msg.wParam, msg.lParam);
					ev.setKey(msg.wParam, true);
					break;
				case WM_KEYUP:	// Catch key presses.
				case WM_SYSKEYUP:
					msg.wParam = handleKeyHandedness(msg.wParam, msg.lParam);
					ev.setKey(msg.wParam, false);
					break;
				case WM_SIZE:
					/* If the window gets resized, deal with it. */
					display->resize(LOWORD(msg.lParam), HIWORD(msg.lParam));
					cam.setFOV(display->getWidth(), display->getHeight(), 90.0f);
					glm::perspective(cam.getVFOV(), display->getAspect(), 1.0f, 200.0f);
					break;
				default:
					/* For anything else, let the window handle it. */
					TranslateMessage(&msg);
					DispatchMessage(&msg);
				}
			}
			if (ev.isGamepadConnected()
				&& ev.getGamepadButton(PAD_START))
					done = true;	// Quit the game if start is pressed.

			/* Toggle between fullscreen and windowed mode. */
			if (ev.getKey(KEY_F2)) {
				ev.setKey(KEY_F2, false);
				if (display->isFullScreen()) {
					display->makeWindowed();
				} else {
					display->makeFullscreen();
				}
			}

			while (accum >= dt) {
				ev.setMouseLocked(display->isMouseLocked());
				display->getMousePos(mousePos);
				ev.setMouseX(mousePos[0]); ev.setMouseY(mousePos[1]);
				world.update(dt, timeScale);
				t += dt;
				accum -= dt;
				if (!ev.isMousePosSet()) {
					display->setMousePos(ev.getMouseX(), ev.getMouseY());
					ev.setMousePosSet(true);
				}
			}
			
			/* Calculate the predition scalar for the next frame. */
			const float interp = (float)(accum / dt);
			/* Render the predicted state of each entity. */
			display->update(); // Clear the display.
			world.render(interp);
			Drawable::getGLError("");
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
			glBindVertexArray(0);
			glUseProgram(0)	;
		}
		delete display; // Delete the GLDisplay pointer.
		#ifdef _DEBUG
		con.close();
		#endif
		/* As a window existed, let Windows deal with the exit code. */
		return (int)(msg.wParam);
	} else {
		/* Show this just in case the display fails to start. */
		MessageBox(NULL, "Failed to initialise display!", "Critical Error!", MB_OK | MB_ICONERROR);
	}
	#ifdef _DEBUG
	con.close();
	#endif
	/* If the display creation fails, just exit with this error code. */
	delete display;
	return -1;
}