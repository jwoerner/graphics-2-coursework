#include "../include/ParentedSpot.h"

#include "../include/glm/gtx/vector_angle.hpp"

ParentedSpot::ParentedSpot(Entity &parent) : owner(parent), 
	offset(glm::vec3(1.0f)), dirOffset(glm::vec3(0.0f)) {

		Spotlight();
}

ParentedSpot::ParentedSpot(Entity &parent, glm::vec3 p, glm::vec3 dOff, 
	float size, float multiplier) : owner(parent), dirOffset(glm::vec3(0.0f)) 
{
	pos = glm::vec4(p, 1.0f);
	spec = glm::vec3(1.0f);
	spotSize = size;
	spotCutoff = spotSize * 1.5f;
	spotMult = multiplier;
	offset = p;
	dirOffset = dOff;
}

ParentedSpot::ParentedSpot(Entity &parent, glm::vec3 p, glm::vec3 c, 
	glm::vec3 s, glm::vec3 dOff, float size, float multiplier, 
	float r) : owner(parent) {

		pos = glm::vec4(p, 1.0f);
		col = c;
		spec = c;
		radius = r;
		offset = p;
		dirOffset = dOff;
		spotSize = size;
		spotCutoff = spotSize * 1.5f;
		spotMult = multiplier;
}

void ParentedSpot::update(float dt, float timeScale) {

	spotDir = glm::zero<glm::vec3>();

	spotDir.x -= owner.getState().fwd.x;
	spotDir.y += owner.getState().fwd.y;
	spotDir.z += owner.getState().fwd.z;

	spotDir = glm::rotate(spotDir, dirOffset.x, glm::vec3(1.0f, 0.0f, 0.0f));
	spotDir = glm::rotate(spotDir, dirOffset.y, glm::vec3(0.0f, 1.0f, 0.0f));
	spotDir = glm::rotate(spotDir, dirOffset.z, glm::vec3(0.0f, 0.0f, 1.0f));

	pos = glm::vec4(offset, 1.0f);
	pos = glm::rotate(pos, -owner.getState().rot.y, owner.getState().up);
	pos += glm::vec4(owner.getState().pos, 1.0f);

	//std::printf("X : %4.2f, Y: %4.2f, Z: %4.2f\n", spotDir.x, spotDir.y, spotDir.z);
	//std::printf("On: %d\n", enabled);
}