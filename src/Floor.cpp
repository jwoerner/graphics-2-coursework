#include "../include/Floor.h"

Floor::Floor(glm::vec2 worldSize) : size(worldSize) {

	state.pos = glm::zero<glm::vec3>();
	state.rot = glm::zero<glm::vec3>();
	scale = glm::vec2(1.0f);

	mat.setShininess(15.0f);
}

Floor::Floor(glm::vec2 worldSize, glm::vec2 s) : size(worldSize) {

	state.pos = glm::zero<glm::vec3>();
	state.rot = glm::zero<glm::vec3>();
	scale = s;

	mat.setShininess(15.0f);
}

void Floor::setup(Shader& shad, std::vector<Texture*> &textures, 
	std::string texture) {

		bool grassTexFound = false;
		bool specGrassTexFound = false;
		bool roadTexFound = false;
		bool specRoadTexFound = false;
		bool mapTexFound = false;
		std::string grassTexName("grass.tga");
		std::string grassSTexName("grass_s.tga");
		std::string roadTexName("road.tga");
		std::string roadSTexName("road_s.tga");
		texture.append(".tga");
		/* Search the provided textures vector for the requested texture. */
		for (unsigned int i = 0; i < textures.size(); i++) {
			Texture &t = *textures.at(i);
			if (t.getTextureName() == texture) {
				mapTexFound = true;
				roadMap = t.getTextureID();
				break;
			}
			if (t.getTextureName() == grassTexName) {
				grassTexFound = true;
				grassTex = t.getTextureID();
				break;
			}
			if (t.getTextureName() == grassSTexName) {
				specGrassTexFound = true;
				grassTex_s = t.getTextureID();
				break;
			}
			if (t.getTextureName() == roadTexName) {
				roadTexFound = true;
				roadTex = t.getTextureID();
				break;
			}
			if (t.getTextureName() == roadSTexName) {
				specRoadTexFound = true;
				roadTex_s = t.getTextureID();
				break;
			}
		}
		/* If the texture is not found, create it and add it to the vector. */
		if (!mapTexFound) {
			textures.push_back(new Texture(TGA_MIPMAPS | TGA_ALPHA)); 
			mapTexFound = textures.back()->loadTexture(texture); 
			roadMap = textures.back()->getTextureID();
		}
		if (!grassTexFound) {
			textures.push_back(new Texture(TGA_MIPMAPS | TGA_ALPHA)); 
			grassTexFound = textures.back()->loadTexture(grassTexName); 
			grassTex = textures.back()->getTextureID();
		}
		if (!specGrassTexFound) {
			textures.push_back(new Texture(TGA_MIPMAPS | TGA_ALPHA)); 
			specGrassTexFound = textures.back()->loadTexture(grassSTexName); 
			grassTex_s = textures.back()->getTextureID();
		}
		if (!roadTexFound) {
			textures.push_back(new Texture(TGA_MIPMAPS | TGA_ALPHA)); 
			roadTexFound = textures.back()->loadTexture(roadTexName); 
			roadTex = textures.back()->getTextureID();
		}
		if (!specGrassTexFound) {
			textures.push_back(new Texture(TGA_MIPMAPS | TGA_ALPHA)); 
			roadSTexName = textures.back()->loadTexture(roadSTexName); 
			roadTex_s = textures.back()->getTextureID();
		}
		Plane::setup(shad);
}

void Floor::render(const glm::mat4 &view, const glm::mat4 &proj, 
		const std::vector<Light*> &lights, const Fog &fog, 
		const float dt) {

			glm::mat4 mvMat(1.0f);
			mvMat = glm::rotate(mvMat, state.rot.x, glm::vec3(1.0f, 0.0f, 0.0f));
			mvMat = glm::rotate(mvMat, state.rot.y, glm::vec3(0.0f, 1.0f, 0.0f));
			mvMat = glm::rotate(mvMat, state.rot.z, glm::vec3(0.0f, 0.0f, 1.0f));
			mvMat = glm::translate(mvMat, state.pos);

			Drawable::prepare(mvMat, view, proj, lights, fog);
			Drawable::getGLError("Floor prepare");

			//GLint mapLoc = shader.getUniform("diffuseTex");
			GLint sizeLoc = shader.getUniform("size");
			GLint mapLoc = shader.getUniform("worldMap");
			GLint grassDLoc = shader.getUniform("grassTexDiffuse");
			GLint grassSLoc = shader.getUniform("grassTexSpecular");
			GLint roadDLoc = shader.getUniform("roadTexDiffuse");
			GLint roadSLoc = shader.getUniform("roadTexSpecular");
			Drawable::getGLError("Floor location");
			glUniform2fv(sizeLoc, 1, &size[0]);Drawable::getGLError("F0");
			glUniform1i(mapLoc, 0);
			glUniform1i(grassDLoc, 1);
			glUniform1i(grassSLoc, 2);
			glUniform1i(roadDLoc, 3);
			glUniform1i(roadSLoc, 4);
			Drawable::getGLError("Floor uniform setting");
			/* Upload floor map. */
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, roadMap);
			/* Upload floor diffuse textures. */
			if (grassDLoc > -1 && roadDLoc > -1) {
				glActiveTexture(GL_TEXTURE1);
				glBindTexture(GL_TEXTURE_2D, grassTex);

				glActiveTexture(GL_TEXTURE3);
				glBindTexture(GL_TEXTURE_2D, roadTex);
			}
			/* Upload floor specular textures. */
			if (grassSLoc > -1 && roadSLoc > -1) {
				glActiveTexture(GL_TEXTURE2);
				glBindTexture(GL_TEXTURE_2D, grassTex_s);

				glActiveTexture(GL_TEXTURE4);
				glBindTexture(GL_TEXTURE_2D, roadTex_s);
			}
			Drawable::getGLError("Floor upload");
			
			Drawable::render();
			Drawable::getGLError("Floor render");
}

Floor::~Floor() {


}