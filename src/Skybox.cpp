#include "../include/Skybox.h"

#include "../include/glm/gtc/matrix_transform.hpp"
#include "../include/utils/tiny_obj_loader.h"

#include <iostream>
#include <sstream>

Skybox::Skybox(Camera& c) : cam(c), time(0.0f), day(true) { }

void Skybox::setup(Shader& shad, std::vector<Texture*> &textures, std::string texture) {

	std::vector<tinyobj::shape_t> shapes;
	tinyobj::LoadObj(shapes, "resource/models/sky.obj");
	tinyobj::shape_t mesh = shapes[0];

	verts	= std::vector<GLfloat>(mesh.mesh.positions);
	norms	= std::vector<GLfloat>(mesh.mesh.normals);
	tex		= std::vector<GLfloat>(mesh.mesh.texcoords);
	index	= std::vector<GLuint>(mesh.mesh.indices);
	
	vertCount = verts.size();
	indexCount = index.size();

	bool texFound = false;
	bool texFoundNight = false;
	std::string nightTex(texture);
	nightTex.append("_n.tga");
	texture.append(".tga");
	/* Search the provided textures vector for the requested texture. */
	for (unsigned int i = 0; i < textures.size(); i++) {
		Texture &t = *textures.at(i);
		if (t.getTextureName() == texture) {
			texFound = true;
			skyTex = t.getTextureID();
			break;
		}
		if (t.getTextureName() == nightTex) {
			texFoundNight = true;
			skyTexN = t.getTextureID();
			break;
		}
	}
	/* If the texture is not found, create it and add it to the vector. */
	if (!texFound) {
		std::stringstream newTex;
		newTex << "skybox/" << texture;

		textures.push_back(new Texture(TGA_MIPMAPS | TGA_ALPHA)); 
		texFound = textures.back()->loadTexture(newTex.str()); 
		skyTex = textures.back()->getTextureID();
	}
	if (!texFoundNight) {
		std::stringstream newTex;
		newTex << "skybox/" << nightTex;

		textures.push_back(new Texture(TGA_MIPMAPS | TGA_ALPHA)); 
		texFoundNight = textures.back()->loadTexture(newTex.str()); 
		skyTexN = textures.back()->getTextureID();
	}

	Drawable::setup(shad);
}

void Skybox::update(float dt, float timeScale) {

	time += 5.0f * timeScale * dt;
	if (time >= 360.0f) { time -= 360.0f; day = !day; }

	//std::printf("%1.2f\n", time);
}

void Skybox::render(const glm::mat4& view, const glm::mat4& proj, std::vector<Light*>& lights, const Fog &fog) {

	/* Move the skybox to the view position. */
	glm::mat4 mvMat = glm::translate(glm::mat4(1.0f), cam.getPos());
	if (fog.type != 0)
		mvMat = glm::scale(mvMat, glm::vec3(75.0f)); // Scale up for fog.
	Drawable::prepare(mvMat, view, proj, lights, fog);
	Drawable::getGLError("Skybox mat upload");

	GLint timeLoc = shader.getUniform("time");
	GLint dayLoc = shader.getUniform("isDay");
	GLint texLoc = shader.getUniform("day");
	GLint texLocN = shader.getUniform("night");

	glUniform1f(timeLoc, time);
	glUniform1i(dayLoc, day);
	glUniform1i(texLoc, 1);
	glUniform1i(texLocN, 0);
	/* Upload skybox texture. */
	if (texLoc > -1 && texLocN > -1) {
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, skyTex);

		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, skyTexN);
	}
	Drawable::getGLError("Skybox tex upload");
	if (fog.type == 0 || fog.density <= 0.001f)
		glDepthMask(GL_FALSE); // Render behind everything.
	Drawable::render();
	if (fog.type == 0 || fog.density <= 0.001f)
		glDepthMask(GL_TRUE);
	Drawable::getGLError("Skybox render");
}

Skybox::~Skybox() {}