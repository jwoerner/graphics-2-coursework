#include "../include/PhysicsSystem.h"

#include <algorithm>
#include <iostream>
#include <limits>

bool PhysicsSystem::boundSphereIntersection(glm::vec3 c1, glm::vec3 c2, 
	float r1, float r2) {

	glm::vec3 distance = c1 - c2;
	float distSq = (distance.x * distance.x) + (distance.y * distance.y) + (distance.z * distance.z);
	float radiusSq = (r1 + r2) * (r1 + r2);
	return (fabs(distSq) <= fabs(radiusSq));
}

bool PhysicsSystem::isIntersecting(std::vector<glm::vec3> &shape1, 
		std::vector<glm::vec3> &shape2, std::vector<glm::vec3> &simplex) {

	/* Pick a point to start testing from and get the first direction. */
	glm::vec3 startPoint = shape1.at(0) - shape2.at(0);
	glm::vec3 S = maxPoint(shape1, shape2, startPoint);
	glm::vec3 dir = -S;

	simplex.push_back(S);

	for (int i = 0; i < MAX_ATTEMPTS; i++) {
		glm::vec3 A = maxPoint(shape1, shape2, dir);

		if (glm::dot(A, dir) < 0.0f) {
			return false; // No intersection.
		}

		simplex.push_back(A);

		if (getSimplex(simplex, dir)) {
			return true; // Intersection found.
		}
	}

	return false; // No intersection.
}

bool PhysicsSystem::getSimplex(std::vector<glm::vec3> &simplex, glm::vec3 &dir) {

	glm::vec3 a, b, c, d;	// Possible points to test against.
	glm::vec3 ao, ab, ac, ad;	// Possible edges to test against.
	glm::vec3 abc, acd, adb;	// Possible planes to test against.

	std::vector<glm::vec3>::iterator vec3It;

	switch (simplex.size()) {
	case 2:	// The simplex is a line (1D).
		/* Get the points of the simplex. */
		a = simplex.at(1);
		b = simplex.at(0);
		/* Calculate the line between the points. */
		ao = -a;
		ab = b - a;

		if (sameDirection(ab, ao)) {
			dir = glm::cross(glm::cross(ab, ao), ab);
		} else {
			dir = ao;
		}
		break;
	case 3:	// The simplex is a triangle (2D).
		/* Get the points of the simplex. */
		a = simplex.at(2);
		b = simplex.at(1);
		c = simplex.at(0);
		/* Calculate the lines between the points. */
		ao = -a;
		ab = b - a;
		ac = c - a;
		/* Calculate the plane between the lines. */
		abc = glm::cross(ab, ac);

		if (sameDirection(glm::cross(abc, ac), ao)) {
			if (sameDirection(ac, ao)) {
				simplex.clear(); // Remove all glm::vec3 points.
				simplex.push_back(c); // Add the useful points back.
				simplex.push_back(a);
				dir = glm::cross(glm::cross(ac, ao), ac);
			} else {
				if (sameDirection(ab, ao)) {
					simplex.clear();
					simplex.push_back(b);
					simplex.push_back(a);

					dir = glm::cross(glm::cross(ab, ao), ab);
				} else {
					simplex.clear();
					simplex.push_back(a);
					dir = ao;
				}
			}
		} else {
			if (sameDirection(glm::cross(ab, abc), ao)) {
				if (sameDirection(ab, ao)) {
					simplex.clear();
					simplex.push_back(b);
					simplex.push_back(a);

					dir = glm::cross(glm::cross(ab, ao), ab);
				} else {
					simplex.clear();
					simplex.push_back(a);
					dir = ao;
				}
			} else {
				if (sameDirection(abc, ao)) {
					dir = abc;
				} else {
					simplex.clear();
					simplex.push_back(b);
					simplex.push_back(c);
					simplex.push_back(a);

					dir = -abc;
				}
			}
		}
		break;
	case 4: // The simplex is a tetrahedron (3D).
		/* Get the points of the simplex. */
		a = simplex.at(3);
		b = simplex.at(2);
		c = simplex.at(1);
		d = simplex.at(0);
		/* Calculate the lines between the points. */
		ao = -a;
		ab = b - a;
		ac = c - a;
		ad = d - a;
		/* Calculate the planes between the lines. */
		abc = glm::cross(ab, ac);
		acd = glm::cross(ac, ad);
		adb = glm::cross(ad, ab);

		/* Work out what side of each plane the points are. */
		float bSideACD = glm::sign(glm::dot(acd, ab));
		float cSideADB = glm::sign(glm::dot(adb, ac));
		float dSideABC = glm::sign(glm::dot(abc, ad));
		/* Check if these points are the same side as the Origin. */
		bool abOriginSide = glm::sign(glm::dot(acd, ao)) == bSideACD;
		bool acOriginSide = glm::sign(glm::dot(adb, ao)) == cSideADB;
		bool adOriginSide = glm::sign(glm::dot(abc, ao)) == dSideABC;

		if (abOriginSide && acOriginSide && adOriginSide) return true; // There's an intersection.

		/* Remove the simplex point that isn't on the same side as the Origin. */
		if (!abOriginSide) {
			vec3It = std::find(simplex.begin(), simplex.end(), b);
			vec3It = simplex.erase(vec3It);

			dir = (acd * (-bSideACD));
		} else if (!acOriginSide) {
			vec3It = std::find(simplex.begin(), simplex.end(), c);
			vec3It = simplex.erase(vec3It);

			dir = (adb * (-cSideADB));
		} else if (!adOriginSide) {
			vec3It = std::find(simplex.begin(), simplex.end(), d);
			vec3It = simplex.erase(vec3It);

			dir = (abc * (-dSideABC));
		}

		/* Recursively attempt to find a simplex that contains the Origin. */
		return getSimplex(simplex, dir);
		break;
	}
	/* No intersection found this iteration. */
	return false;
}

glm::vec3 PhysicsSystem::maxPoint(const std::vector<glm::vec3> &shape, glm::vec3 dir) {

	glm::vec3 max = shape.at(0);
	std::vector<glm::vec3>::const_iterator vec3It;
	/* Check the stored point against every other point. */
	for (vec3It = shape.begin(); vec3It != shape.end(); vec3It++) {
		if (glm::dot(max, dir) < glm::dot(*vec3It, dir)) {
			max = *vec3It; // The new furthest point in the direction.
		}
	}

	return max;
}

glm::vec3 PhysicsSystem::maxPoint(const std::vector<glm::vec3> &shape1, const std::vector<glm::vec3> &shape2, glm::vec3 dir) {

	/* Calculate the difference between the max points of each shape in opposide directions. */
	return maxPoint(shape1, dir) - maxPoint(shape2, -dir);
}

glm::vec3 PhysicsSystem::getIntersection(std::vector<glm::vec3> &shape1, 
		std::vector<glm::vec3> &shape2, std::vector<glm::vec3> &simplex) {

	glm::vec3 result;

	for (int i = 0; i < MAX_ATTEMPTS; i++) {
		Face e = closestFace(simplex);

		glm::vec3 furthestPoint = maxPoint(shape1, shape2, e.normal);
		float dist = glm::dot(furthestPoint, e.normal);

		if ((dist + e.originDistance) < std::numeric_limits<float>::epsilon()) {
			/* If this is a good resolution, just take it. */
			result = dist * e.normal; // Return this result.
			break;
		} else {
			int num = std::count(simplex.begin(), simplex.end(), furthestPoint);
			if (num == 0) {
				/* If this isn't a good resolution, add to the simplex and try again. */
				simplex.insert(simplex.begin() + e.index, furthestPoint);
			}
		}
	}
	return result;
}

Face PhysicsSystem::closestFace(std::vector<glm::vec3> &simplex) {

	Face e;
	e.originDistance = std::numeric_limits<float>::max();

	for (unsigned int i = 0; i < simplex.size(); ++i) {
		unsigned int j = (i + 1) >= simplex.size() ? 0 : i + 1;
		unsigned int k = (j + 1) >= simplex.size() ? 0 : j + 1;

		glm::vec3 a = simplex.at(k);
		glm::vec3 b = simplex.at(j);
		glm::vec3 c = simplex.at(i);
		/* Calculate the edges between the points. */
		glm::vec3 ao = -a;
		glm::vec3 ab = b - a;
		glm::vec3 ac = c - a;

		glm::vec3 abc = glm::cross(ab, ac);

		glm::vec3 dir = glm::cross(glm::cross(abc, ao), abc);

		dir = glm::normalize(dir);
		float dist = glm::dot(a, dir);

		if (dist < e.originDistance) {
			e.originDistance = dist;
			e.normal = dir;
			e.index = k;
		}
	}

	return e;
}

glm::vec3 PhysicsSystem::getMTV(std::vector<glm::vec3> &simplex) {

	glm::vec3 min = simplex.at(0);
	glm::vec3 dir = -min;

	std::vector<glm::vec3>::iterator it;
	/* Test for the closest vertex. */
	for (unsigned int i = 0; i < simplex.size(); i++) {
		glm::vec3 test = simplex.at(i);
		glm::vec3 testDir = -test;
		if (glm::dot(test, testDir) < glm::dot(min, dir)) {
			min = test;
			dir = testDir;
		}
	}
	/* Test for the closest edge. */
	for (unsigned int i = 0; i < simplex.size(); i++) {
		glm::vec3 a = simplex.at(i);
		glm::vec3 b = i + 1 >= simplex.size() ? simplex.at((i + 1) % simplex.size()) : simplex.at(i+1);

		glm::vec3 ao = -a;
		glm::vec3 ab = b - a;

		glm::vec3 normal = glm::normalize(glm::cross(glm::cross(ab, ao), ab));
			
		if (glm::dot(normal, ao) < glm::dot(min, dir)) {
			min = ab;
			dir = ao;
		}
	}
	/* Test for the closest face. */
	for (unsigned int i = 0; i < simplex.size(); i++) {

	}

	return min;
}