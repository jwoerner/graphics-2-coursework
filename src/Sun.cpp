#include "../include/Sun.h"

#include "../include/glm/gtc/constants.hpp"

#include <iostream>

Sun::Sun(float t) : dayCol(1.0f, 0.0f, 0.0f), nightCol(0.0f, 0.0f, 1.0f), 
	axis(0.0f, 1.0f), time(t), day(true) { 
	
	pos = glm::vec4(1.0f, 1.0f, 1.0f, 0.0f);
	curCol = dayCol; 
	enabled = true;
}

Sun::Sun(glm::vec3 dCol, glm::vec3 nCol, glm::vec2 a, float t) : dayCol(dCol), 
	nightCol(nCol), axis(a), time(t), day(true) {

	pos = glm::vec4(1.0f, 1.0f, 1.0f, 0.0f);
	curCol = dayCol; 
	enabled = true;
}

void Sun::update(float dt, float timeScale) {

	time += 5.0f * timeScale * dt;
	if (time >= 360.0f) { time -= 360.0f; day = !day; }

	pos.x += (glm::cos(-time * (glm::pi<float>() / 180.0f)) * timeScale * axis.x);
	pos.z += (glm::cos(-time * (glm::pi<float>() / 180.0f)) * timeScale * axis.y);
	pos.y += (glm::sin(time * (glm::pi<float>() / 180.0f)) * timeScale);

	float colDiff = time / 180.0f;
	if (time > 180.0f) {
		/* Count backwards if it's past midday. */
		float diffFloor = glm::floor((colDiff));
		colDiff = diffFloor - glm::modf(colDiff, diffFloor);
	}
	//std::printf("X: %4.2f, Y: %4.2f, Z: %4.2f, Time: %5.0f\n", pos.x, pos.y, pos.z, time);
	//std::printf("R: %4.2f, G: %4.2f, B: %4.2f, Time: %3.0f, %3.2f\n", curCol.x, curCol.y, curCol.z, time, colDiff);

	if (day)
		curCol = dayCol * colDiff + (1 - colDiff) * glm::zero<glm::vec3>();
	else 
		curCol = nightCol * colDiff + (1 - colDiff) * glm::zero<glm::vec3>();
	
	col = curCol;
	spec = curCol;
}
