#include "../include/Scene.h"

#include "../include/Collidable.h"
#include "../include/ParentedSpot.h"
#include "../include/Sun.h"
#include "../include/Streetlight.h"
#include "../include/StaticProp.h"
#include "../include/types/Shader.h"

#include "../include/utils/tiny_obj_loader.h"

#include <iostream>

Scene::Scene(InputHandler *e, Camera &c) : ev(e), cam(c), sky(cam), floor(glm::vec2(128.0f), glm::vec2(128.0f)) {}

void Scene::init() {

	fog.startDist = 25.0f;
	fog.endDist = 50.0f;
	fog.density = 0.05f;
	fog.type = FOG_EXP;

	std::string path("resource/models/");
	std::string file = "resource/models/house.obj";
	loadModel(file, path);
	ents.push_back(static_cast<Entity*>(new StaticProp(glm::vec3(-5.0f, 0.0f, 0.0f), glm::vec3(0.0f, 60.0f, 0.0f))));
	ents.back()->setMesh(*meshes.back());
	file = "resource/models/lamppost.obj";
	loadModel(file, path);
	ents.push_back(static_cast<Entity*>(new Streetlight(glm::vec3(35.0f, 0.0, -30.0f), glm::vec3(0.0f, -30.0f, 0.0f))));
	ents.back()->setMesh(*meshes.back());
	ents.push_back(static_cast<Entity*>(new Streetlight(glm::vec3(35.0f, 0.0, 30.0f), glm::vec3(0.0f, 30.0f, 0.0f))));
	ents.back()->setMesh(*meshes.back());
	ents.push_back(static_cast<Entity*>(new Streetlight(glm::vec3(12.0f, 0.0, 15.0f), glm::vec3(0.0f, 180.0f, 0.0f))));
	ents.back()->setMesh(*meshes.back());
	ents.push_back(static_cast<Entity*>(new Streetlight(glm::vec3(12.0f, 0.0, -15.0f), glm::vec3(0.0f, 180.0f, 0.0f))));
	ents.back()->setMesh(*meshes.back());
	ents.push_back(static_cast<Entity*>(new Streetlight(glm::vec3(26.0f, 0.0, 0.0f), glm::vec3(0.0f))));
	ents.back()->setMesh(*meshes.back());
	/* Create the Player. */
	ply = new Player(*ev);
	path = "resource/models/player/";
	file = "resource/models/player/body.obj";
	loadModel(file, path);
	ply->setMesh(*meshes.back());
	ply->setWorldLimit(floor.getSize());
	// Add in a Sun.
	lights.push_back(new Sun(glm::vec3(1.0f, 0.98f, 0.91f), glm::vec3(0.0f, 0.15f, 0.15f), glm::vec2(0.0f, 1.0f)));

	shaders.push_back(new Shader);	
	if (shaders.back()->load("skybox", "resource/shaders/skybox.vert", "resource/shaders/skybox.frag")) {
		sky.setup(*shaders.back(), tex, "sky");
	}
	shaders.push_back(new Shader);	
	//if (shaders.back()->load("slow", "resource/shaders/diffspecnor.vert", "resource/shaders/diffspecnor.frag")) {
	if (shaders.back()->load("diffspectex", "resource/shaders/basicTransformations.vert", "resource/shaders/diffspectex.frag")) {
		for (meshIt = meshes.begin(); meshIt != meshes.end(); meshIt++) {
			if ((*meshIt)->getName() == "player" || (*meshIt)->getName() == "lamppost") {
				(*meshIt)->setup(*shaders.back());
			}
		}
		
	}
	shaders.push_back(new Shader);	
	//if (shaders.back()->load("slow", "resource/shaders/diffspecnor.vert", "resource/shaders/diffspecnor.frag")) {
	if (shaders.back()->load("diff", "resource/shaders/basicTransformations.vert", "resource/shaders/diff.frag")) {
	//if (shaders.back()->load("fast", "resource/shaders/fast_diffspec.vert", "resource/shaders/fast_diffspec.frag")) {
		for (meshIt = meshes.begin(); meshIt != meshes.end(); meshIt++) {
			if ((*meshIt)->getName() != "player" && (*meshIt)->getName() != "lamppost") {
				(*meshIt)->setup(*shaders.back());
			}
		}
	}
	/* Setup shader for ground. */
	shaders.push_back(new Shader);
	if (shaders.back()->load("floor", "resource/shaders/floor.vert", "resource/shaders/floor.frag")) {
		floor.setup(*shaders.back(), tex, "map");
	}

	for (entsIt = ents.begin(); entsIt != ents.end(); entsIt++) {
		(*entsIt)->init();
	}

	ply->init();
	ents.push_back(static_cast<Entity*>(ply));

	cam.setTarget(*ply);
}

void Scene::update(float dt, float timeScale) {
	
	if (ev->getKey(KEY_RSHFT)
		|| (ev->isGamepadConnected()
		&& ev->getGamepadButton(PAD_RB)))
			timeScale = 5.0; // Increse the timescale.

	sky.update(dt, timeScale);
	std::vector<Collidable*> collisionMeshes;
	for (entsIt = ents.begin(); entsIt != ents.end(); entsIt++) {
		Collidable* c;
		c = dynamic_cast<Collidable*>(*entsIt);
		if (c != NULL) {
			/* If the thing is collidable, and is close enough to the player. */
			if (glm::length(c->getCenter() - ply->getCenter()) < 20.0f)
				/* Poor-mans broad phase. Oh well. */
				collisionMeshes.push_back(c); // Check it.
		}
	}
	/* Perform collision detection on all entities. */
	for (entsIt = ents.begin(); entsIt != ents.end(); entsIt++) {
		(*entsIt)->update(dt, timeScale);
		Collidable* c;
		c = dynamic_cast<Collidable*>(*entsIt);
		if (c != NULL) {
			/* If this Entity is also Collidable, check for collisions. */
			c->checkCollisions(collisionMeshes);
		}
		(*entsIt)->move(dt);
	}
	/* Update all the lights. */
	for (lightIt = lights.begin(); lightIt != lights.end(); lightIt++) {
		if ((*lightIt)->isEnabled()) {
			(*lightIt)->update(dt, timeScale);
		}
	}
	glm::vec3 newFloorPos = glm::vec3(ply->getState().pos.x - (floor.getSize().x / 2.0f), 0.0f, ply->getState().pos.z - (floor.getSize().y / 2.0f));
	floor.setPos(newFloorPos);
	cam.update();
	if (sky.isDay()) {
		fog.colour = glm::vec4(lights.at(0)->getCol()/2.0f, 1.0f);
	} else {
		fog.colour = glm::vec4(lights.at(0)->getCol()/2.0f, 1.0f);
	}
}

void Scene::render(float interp) {
	
	std::vector<Light*> tmpLights;
	tmpLights.insert(tmpLights.begin(), lights.begin(), lights.end());
	/* Get all the lights from entities. */
	for (entsIt = ents.begin(); entsIt != ents.end(); entsIt++) {
		tmpLights.insert(tmpLights.begin(), 
			(*entsIt)->getLights().begin(), (*entsIt)->getLights().end());
	}

	glm::mat4 view = cam.getView();
	sky.render(view, proj, tmpLights, fog);
	Drawable::getGLError("sky render");
	floor.render(view, proj, tmpLights, fog, interp);
	for (entsIt = ents.begin(); entsIt != ents.end(); entsIt++) {
		if (glm::length((*entsIt)->getState().pos - ply->getState().pos) > 75.0f)
			continue; // Poor mans occlusion.
		/* Check to see if the entity has a renderable component. */
		(*entsIt)->render(view, proj, tmpLights, fog, interp);
	}
	Drawable::getGLError("ents render");
}

void Scene::loadModel(std::string f, std::string p) {

	std::vector<tinyobj::shape_t> shapes;
	tinyobj::LoadObj(shapes, f.c_str(), p.c_str());
	meshes.push_back(new Mesh);

	Material mat;
	mat.setAmbient(glm::vec3(shapes.at(0).material.ambient[0], shapes.at(0).material.ambient[1], shapes.at(0).material.ambient[2]));
	mat.setDiffuse(glm::vec3(shapes.at(0).material.diffuse[0], shapes.at(0).material.diffuse[1], shapes.at(0).material.diffuse[2]));
	mat.setSpecular(glm::vec3(shapes.at(0).material.specular[0], shapes.at(0).material.specular[1], shapes.at(0).material.specular[2]));
	mat.setShininess(shapes.at(0).material.shininess);
	/* Load the textures specified by the mtl file. */
	std::string ta, td, ts, tn;
	ta = std::string(shapes.at(0).material.ambient_texname);
	td = std::string(shapes.at(0).material.diffuse_texname);
	ts = std::string(shapes.at(0).material.specular_texname);
	tn = std::string(shapes.at(0).material.normal_texname);

	bool hasTA, hasTD, hasTS, hasTN;
	hasTA = ta.empty(); hasTD = td.empty(); hasTS = ts.empty(); hasTN = tn.empty();

	if (!ta.empty() || !td.empty() || !ts.empty() || !tn.empty()) {
		for (texIt = tex.begin(); texIt != tex.end(); texIt++) {
			/* Exit the loop if all textures were found. */
			if (hasTA && hasTD && hasTS && hasTN) break;
			/* Otherwise check each texture against the current one. */
			if (!hasTA && ta == (*texIt)->getTextureName()) {
				/* If the texture exists already, use that ID and stop looking for it. */
				mat.setAmbientTexture((*texIt)->getTextureID());
				hasTA = true;
			}
			if (!hasTD && td == (*texIt)->getTextureName()) {
				mat.setDiffuseTexture((*texIt)->getTextureID());
				hasTD = true;
			}
			if (!hasTS && ts == (*texIt)->getTextureName()) {
				mat.setSpecularTexture((*texIt)->getTextureID());
				hasTS = true;
			}
			if (!hasTN && tn == (*texIt)->getTextureName()) {
				mat.setNormalTexture((*texIt)->getTextureID());
				hasTN = true;
			}
		}
	}
	/* Add textures that weren't found to the vector. */
	if (!hasTA && !ta.empty()) { tex.push_back(new Texture(TGA_ALPHA | TGA_MIPMAPS)); tex.back()->loadTexture(ta); mat.setAmbientTexture(tex.back()->getTextureID()); }
	if (!hasTD && !td.empty()) { tex.push_back(new Texture(TGA_ALPHA | TGA_MIPMAPS)); tex.back()->loadTexture(td); mat.setDiffuseTexture(tex.back()->getTextureID()); }
	if (!hasTS && !ts.empty()) { tex.push_back(new Texture(TGA_ALPHA | TGA_MIPMAPS)); tex.back()->loadTexture(ts); mat.setSpecularTexture(tex.back()->getTextureID()); }
	if (!hasTN && !tn.empty()) { tex.push_back(new Texture(TGA_ALPHA | TGA_MIPMAPS)); tex.back()->loadTexture(tn); mat.setNormalTexture(tex.back()->getTextureID()); }

	meshes.back()->setMesh(shapes.at(0));
	meshes.back()->setMaterial(mat);
}

Scene::~Scene() {

	/* Safely clean up any pointers. */
	/*
	for (entsIt = ents.begin();
		entsIt != ents.end();
		entsIt++) {
			delete (*entsIt);
	}
	for (meshIt = meshes.begin();
		meshIt != meshes.end();
		meshIt++) {
			delete (*meshIt);
	}
	for (texIt = tex.begin();
		texIt != tex.end();
		texIt++) {
			delete (*texIt);
	}
	for (lightIt = lights.begin();
		lightIt != lights.end();
		lightIt++) {
			delete (*lightIt);
	}
	for (shdIt = shaders.begin();
		shdIt != shaders.end();
		shdIt++) {
			delete (*shdIt);
	}
	*/
	/* Clear the vectors entirely. */
	ents.clear();
	meshes.clear();
	tex.clear();
	lights.clear();
	shaders.clear();
}