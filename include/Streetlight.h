#ifndef STREETLIGHT_H
#define STREETLIGHT_H
/******************************************************************************
 *	Name:			Streetlight
 *	Author:			Jordan Woerner
 *	Description:	An Entity that owns a light. Turns on and off autmatically,
					 can be bumped in to and broken by other Collidable objects
					 , causing it to fall over.
 *****************************************************************************/

#include "Collidable.h"
#include "types/Entity.h"

class Streetlight : public Entity, public Collidable
{
public:
	/*
	 Creates a Streetlight at (0, 0, 0) with a white light.
	 */
	Streetlight();

	/*
	 Creates a Streetlight at the specified position with the provided rotation
	  and a white light.
	 */
	Streetlight(glm::vec3 pos, glm::vec3 rot);

	/*
	 Creates a Streetlight at the specified position with the provided rotation
	  and a light of the provided colour.
	 */
	Streetlight(glm::vec3 pos, glm::vec3 rot, glm::vec3 col);

	/*
	 Sets up the collision mesh of this Streetlight and creates the light.
	 */
	void init();

	/*
	 Tracks the time of day and toggles the light in response.
	 */
	void update(float dt, float timescale = 1.0f);

	/*
	 Tilts this Streetlight when something hits it, turning off the light when 
	 it is tilted far enough.
	 */
	void resolveCollision(Collidable* other, std::vector<glm::vec3> &simplex);

	/*
	 Updates the rotation of the Streetlight.
	 */
	void move(float dt = 1.0f);

	/*
	 Draws the model for this Streetlight.
	 */
	void render(const glm::mat4 &view, const glm::mat4 &proj, 
		const std::vector<Light*> &lights, const Fog &fog, 
		const float dt = 0.0f);

private:
	glm::vec3 lightPos;	// Where on the model should the light be.
	glm::vec3 lightCol;	// What colour should the light be.
	float time;			// The time of day.
	bool broken;		// If this Streetlight broken?

};

#endif // STREETLIGHT_H