#ifndef T_MESH_H
#define T_MESH_H
/******************************************************************************
 *	Name:			Mesh
 *	Author:			Jordan Woerner
 *	Description:	Contains the verticies, materials and shading type of a 
					renderable 3D mesh.
 *****************************************************************************/

#include "Material.h"
#include "../Drawable.h"
/* External library headers. */
#include "../gl/glew.h"
#include "../../include/utils/tiny_obj_loader.h"

#include <vector>

#define MESH_FLAT	true
#define MESH_SMOOTH false

class Mesh : public Drawable
{
public:
	/*
	 Creates a new smooth shaded Mesh.
	 */
	Mesh();

	/*
	 Creaste a new Mesh that can be smooth or flat shaded.
	 */
	Mesh(bool meshType);

	void setMesh(tinyobj::shape_t &mesh);

	inline void setMaterial(Material m) { mat = m; }

	/*
	 Sets the name of this Mesh.
	 */
	inline void setName(std::string n) { name = n; }

	/*
	 Changes the shading between flat and smooth.
	 */
	inline void setShading(bool flatOn) { flat = flatOn; }

	/*
	 Gets the name of this Mesh as a String.
	 */
	inline std::string getName() const { return name; }

	/*
	 Loads the specified OBJ model file.
	 */
	void calcNorms();

	/*
	 Sets up the model.
	 */
	void prepare(const glm::mat4 &mvMat, const glm::mat4 &view, const glm::mat4 &proj, const std::vector<Light*> &lights, const Fog &fog);

	~Mesh();

private:
	std::string name;	// A name for the Mesh.
	bool flat;			// True if the mesh is flat shaded.

};

#endif // T_MESH_H