#ifndef LIGHT_SPOT_H
#define LIGHT_SPOT_H
/******************************************************************************
 *	Name:			Spot
 *	Author:			Jordan Woerner
 *	Description:	Specialisation of Light to allow for directed lighting.
 *****************************************************************************/

#include "Light.h"

class Spotlight : public Light
{
public:
	/*
	 Creates a spotlight that points downwards with a size of 60.
	 */
	Spotlight();

	/*
	 Creates a white Spotlight pointing in the specified direction and of the 
	 specified size (clamped 1-90). Multiplier minimum 0.01.
	 */
	Spotlight(glm::vec3 pos, glm::vec3 dir, float spotSize, float multiplier);

	/*
	 Creates a Spotlight of the specified colour pointing in the provided 
	 direction with the specified size (clamped 1-90). Multiplier minimum 0.01.
	 */
	Spotlight(glm::vec3 pos, glm::vec3 col, glm::vec3 spec, glm::vec3 dir, 
		float spotSize, float multiplier, float radius = 1.0f);

	/*
	 Updates this Spotlight.
	 */
	virtual void update(float dt, float timescale = 1.0f) {}

};

#endif // LIGHT_SPOT_H