#ifndef T_MATERIAL_H
#define T_MATERIAL_H
/******************************************************************************
 *	Name:			Material
 *	Author:			Jordan Woerner
 *	Description:	Contains details on the diffuse and specular reflection 
					colours of something. Along with the diffuse, specular and 
					normal map textures to use.
 *****************************************************************************/

#include "../types/Texture.h"

#include "../glm/glm.hpp"

class Material
{
public:
	/*
	 Create a new Material set to white.
	*/
	Material();

	/*
	 Create a new Material with the specified colour and shininess settings.
	 */
	Material(glm::vec3 a, glm::vec3 d, glm::vec3 s, float s2);

	/*
	 Gets the ambient colour of this Material.
	 */
	inline glm::vec3 getAmbient() const { return ambient; }

	/*
	 Sets the ambient colour of this Material.
	 */
	inline void setAmbient(glm::vec3 amb) { ambient = amb; }

	/*
	 Gets the diffuse colour of this Material.
	 */
	inline glm::vec3 getDiffuse() const { return diffuse; }

	/*
	 Sets the diffuse colour of this Material.
	 */
	inline void setDiffuse(glm::vec3 diff) {diffuse = diff; }

	/* 
	 Gets the specular colour of this Material.
	 */
	inline glm::vec3 getSpecular() const { return specular; }

	/*
	 Sets the specular colour of this Material.
	 */
	inline void setSpecular(glm::vec3 spec) { specular = spec; }

	/*
	 Gets the shininess of the Material.
	 */
	inline float getShininess() const { return shininess; }

	/*
	 Sets the shininess of the Material.
	 */
	inline void setShininess(float s) { shininess = s; }

	/*
	 Sets the texture ID for the ambient texture.
	 */
	inline void setAmbientTexture(GLuint tex) { ambientTex = tex; }

	/*
	 Gets the texture ID for the ambient texture. Returns -1 for invalid textures.
	 */
	inline GLuint getAmbientTexID() { return ambientTex; }

	/*
	 Sets the texture ID for the diffuse texture.
	 */
	inline void setDiffuseTexture(GLuint tex) { diffuseTex = tex; }

	/*
	 Gets the texture ID for the diffuse texture. Returns -1 for invalid textures.
	 */
	inline GLuint getDiffuseTexID() { return diffuseTex; }

	/*
	 Sets the texture ID for the specular texture.
	 */
	inline void setSpecularTexture(GLuint tex) { specularTex = tex; }

	/*
	 Gets the texture ID for the specular texture. Returns -1 for invalid textures.
	 */
	inline GLuint getSpecularTexID() { return specularTex; }

	/*
	 Sets the texture ID for the normal texture.
	 */
	inline void setNormalTexture(GLuint tex) { normalTex = tex; }

	/*
	 Gets the texture ID for the normal texture. Returns -1 for invalid textures.
	 */
	inline GLuint getNormalTexID() { return normalTex; }

	/*
	 Uploads the material to the GPU.
	 */
	void uploadMaterial(GLuint shader);

	/*
	 Uploads the stored textures to the GPU.
	 */
	void uploadTextures(GLuint shader);

private:
	glm::vec3 ambient;	// The ambient reflection colour.
	glm::vec3 diffuse;	// The diffuse reflection colour.
	glm::vec3 specular;	// The specular reflection colour.

	GLuint ambientTex;	// The ambient texture.
	GLuint diffuseTex;	// The diffuse texture.
	GLuint specularTex;	// The specular map.
	GLuint normalTex;	// The normal map.

	float shininess;	// The shininess factor of the Material.
};

#endif // T_MATERIAL_H