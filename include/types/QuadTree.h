#ifndef T_QUAD_TREE
#define T_QUAD_TREE
/******************************************************************************
 *	Name:			Quadtree
 *	Author:			Jordan Woerner
 *	Description:	Data structure for storing items in a nested manner.
 *****************************************************************************/

#include "../Collidable.h"

#include "../glm/glm.hpp"

#include <vector>

#define QUADTREE_MAX_LEVELS 5
#define QUADTREE_MAX_ITEMS 1

class QuadTree
{
public:
	/*
	 Creates a new QuadTree with no level or size information.
	 */
	QuadTree();

	/*
	 Creates a new QuadTree starting from the specified level at the provided 
	 position and with cells of the provided size.
	 */
	QuadTree(int level, float x, float y, float w, float h);

	/*
	 Sets the level of this QuadTree node.
	 */
	inline void setLevel(int newLevel) { level = newLevel; }

	/*
	 Sets the X position of this QuadTree node.
	 */
	inline void setXPos(float x) { xPos = x; }

	/*
	 Sets the X size of this QuadTree node.
	 */
	inline void setXBounds(float x) { xSize = x; }

	/*
	 Sets the Y position of this QuadTree node.
	 */
	inline void setYPos(float y) { yPos = y; }

	/*
	 Sets the Y size of this QuadTree node.
	 */
	inline void setYBounds(float y) { ySize = y; }

	/*
	 Divides this node into four children nodes.
	 */
	void split();

	/*
	 Gets the index of the provided bounds in this node. 1 = top left, 
	 2 = bottom left, 3 = top right, 4 = bottom right.
	 */
	int getIndex(const glm::vec3 &min, const glm::vec3 &max) const;

	/*
	 Adds the provided object to this QuadTree.
	 */
	void insert(Collidable* ent);

	/*
	 Gets all the objects held in this node and all the children nodes
	 that overlap with the provided bounds.
	 */
	void retrieve(std::vector<Collidable*> &objects, const glm::vec3 &min, 
		const glm::vec3 &max) const;

private:
	int level;		// The height of this node.
	float xPos;		// The x position of this node.
	float xSize;	// The size of this node on the x axis.
	float yPos;		// The y position of this node.
	float ySize;	// The size of this node on the y axis.
	std::vector<Collidable*> items;	// The items held in this node.
	QuadTree *nodes[4];		// The children of this node.
};

#endif // T_QUAD_TREE