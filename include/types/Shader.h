#ifndef T_SHADER_H
#define T_SHADER_H
/******************************************************************************
 *	Name:			Shader
 *	Author:			Jordan Woerner
 *	Description:	Stores a reference to a OpenGL shader program and Shader 
					objects. Allows a Shader program to be loaded by specifying
					 a vertex and fragment shader to use.
 *****************************************************************************/

#include "../gl/glew.h"

#include <string>

class Shader
{
public:
	/*
	Creates a new Shader object with no name or shaders loaded.
	*/
	Shader();

	/*
	Loads the specified shaders into this Shader (vertex and fragment 
	respectively), and assigns a name to the Shader.
	*/
	bool load(const std::string name, const char* vertFile, 
		const char* fragFile);

	/*
	 Gets the OpenGL reference to a specified shader attribute (in, out, 
	 varying) in this Shader program. Returns -1 if no attribute is found.
	 */
	const GLint getAttrib(const GLchar* attrib) const;

	/*
	 Gets the OpenGL reference to a specified uniform in this Shader program. 
	 Returns -1 if no uniform is found.
	 */
	const GLint getUniform(const GLchar* uni) const;

	/*
	Gets the program ID for this shader.
	*/
	const GLuint getHandle() const { return progID; }

	/*
	Removes this Shader safely and deletes the shader from the GPU.
	*/
	~Shader();

private:
	std::string progName;	// A recognisable name for this shader program.
	GLuint progID;			// The program identifier.
	GLuint vertShader;		// The vertex shader this program uses.
	GLuint fragShader;		// The fragment shader this program uses.

	/*
	Loads the specified shader as the provided type (supported types: 
	GL_VERTEX_SHADER, GL_FRAGMENT_SHADER).
	*/
	GLuint loadShader(const char* shaderFile, const GLenum type);

	/*
	Gets the compile log for the specified shader ID.
	*/
	std::string getShaderLog(const GLuint shader) const;
	
	/*
	Gets the link log for the specified program ID.
	*/
	std::string getProgramLog(const GLuint program) const;
};

#endif // T_SHADER_H