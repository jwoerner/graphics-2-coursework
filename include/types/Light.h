#ifndef LIGHT_H
#define LIGHT_H
/******************************************************************************
 *	Name:			Light
 *	Author:			Jordan Woerner
 *	Description:	Places a light in the game world using the specified colour
					 for diffuse and specular reflections.
 *****************************************************************************/

#include "../glm/glm.hpp"

class Light
{
public:
	/*
	 Constructs a white point light at Origin.
	 */
	Light();

	/*
	 Constructs a white Light at the specified position. The 'w' value can be 
	 set to '1.0f' for directional a light, otherwise the Light is a point 
	 light.
	 */
	Light(glm::vec4 pos);

	/*
	 Creates a Light at the specified position with the specified colour and 
	 specular reflection colour. The 'w' value of the position can be set to 
	 '1.0f' for a directional light, otherwise the Light is a point light.
	 */
	Light(glm::vec4 pos, glm::vec3 col, glm::vec3 spec, float radius = 1.0f);

	/*
	 Nothing to update.
	 */
	virtual void update(float dt, float timescale = 1.0f) {}

	/*
	 Get the position of this Light (including 'w' paramter).
	 */
	inline glm::vec4 getPos() const { return pos; }

	/*
	 Returns the radius of this light.
	 */
	inline float getRadius() const { return radius; }

	/*
	 Turns the light on or off. True is on, false is off.
	 */
	inline void setEnabled(bool on) { enabled = on; }

	/*
	 Switches the state of this light.
	 */
	inline void toggle() { enabled = !enabled; }

	/*
	 Returns true if the light is on, false otherwise.
	 */
	inline bool isEnabled() const { return enabled; }

	/*
	 Gets the direction of the spotlight.
	 */
	inline glm::vec3 getSpotDir() const { return spotDir; }

	/* 
	 Get the diffuse reflection colour for this Light.
	*/
	inline glm::vec3 getCol() const { return col; }

	/*
	 Get the specular reflection colour for this Light.
	 */
	inline glm::vec3 getSpec() const { return spec; }

	/*
	 Get the size of the spotlight in degrees.
	 */
	inline float getSpotSize() const { return spotSize; }
	
	/*
	 Get the size of the spotlight cutoff in degrees.
	 */
	inline float getSpotCutoff() const { return spotCutoff; }

	/*
	 Get the strength of the spotlight.
	 */
	inline float getSpotMult() const { return spotMult; }
	 
	/* 
	 Get the constant attenuation for this Light.
	 */
	inline float getConstantAttenuation() const { return constantAttenuation; }

	/* 
	 Get the linea attenuation for this Light.
	 */
	inline float getLinearAttenuation() const { return linearAttenuation; }

	/* 
	 Get the quadratic attenuation for this Light.
	 */
	inline float getQuadraticAttenuation() const { return quadraticAttenuation; }

protected:
	glm::vec4 pos;	// The direction this light points in.
	glm::vec3 col;	// The colour this light uses for ambient.
	glm::vec3 spec; // Specular colour for the light.
	float radius;	// The radius of a spherical light.
	bool enabled;	// |Is the light on or off?

	glm::vec3 spotDir;	// Direction of the spotlight.
	float spotSize;		// The distance the spotlight should stop emitting full light.
	float spotCutoff;	// The distance the spotlight should be emitting no light.
	float spotMult;		// Power of the spotlight.

	float constantAttenuation;	 // Constant attenuation of the light.
	float linearAttenuation;	 // Linear attenuation of the light.
	float quadraticAttenuation;	 // Quadratic attenuation of the light.

};

#endif // LIGHT_H