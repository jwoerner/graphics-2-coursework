#ifndef T_PHYSICS_STRUCTS_H
#define T_PHYSICS_STRUCTS_H
/******************************************************************************
 *	Name:			PhysicsStructs
 *	Author:			Jordan Woerner
 *	Description:	Holds structs that describe the physical state of something
					 , and allows for them to update.
 *****************************************************************************/

#include "../glm/glm.hpp"

struct Simplex {

	glm::vec3 point;
	bool ownerIsShape1;
	unsigned int index;
};

struct Face {

	glm::vec3 normal;
	float originDistance;
	unsigned int index;
};

struct State {

	glm::vec3 pos;	// Position of the object.
	glm::vec3 rot;	// Rotation of the object.
	glm::vec3 vel;	// Velocity of the object.
	glm::vec3 rotVel;

	glm::vec3 fwd;		// The forward direction of the Player.
	glm::vec3 up;		// The left direction of the Player.
	glm::vec3 right;	// The right direction of the Player.
};

#endif // T_PHYSICS_STRUCTS_H