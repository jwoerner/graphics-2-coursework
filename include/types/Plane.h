#ifndef T_PLANE_H
#define T_PLANE_H
/******************************************************************************
 *	Name:			Plane
 *	Author:			Jordan Woerner
 *	Description:	Represents a surface in 3D geometry.
 *****************************************************************************/

#include "PhysicsStructs.h"
#include "../Collidable.h"
#include "../Drawable.h"

class Plane : public Drawable
{
public:
	/*
	 Creates a flat Plane at (0, 0, 0) of size 1.
	 */
	Plane();

	/*
	 Creates a Plane at the specified position, rotated by the provided angle 
	 (degrees) and of the specified size.
	 */
	Plane(glm::vec3 pos, glm::vec3 rot, glm::vec2 size);

	/*
	 Sets up the geometry of the plane.
	 */
	virtual void setup(Shader& shad);

	inline const State& getState() const { return state; }

	inline void setPos(glm::vec3 pos) { state.pos = pos; }

	inline glm::vec2 getSize() const { return scale; }

	/*
	 Draws the Plane to the screen.
	 */
	virtual void render(const glm::mat4 &view, const glm::mat4 &proj, 
		const std::vector<Light*> &lights, const Fog &fog, 
		const float dt = 0.0f);

protected:
	State state;
	glm::vec2 scale;

};

#endif // T_PLANE_H