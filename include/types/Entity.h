#ifndef ENTITY_H
#define ENTITY_H
/******************************************************************************
 *	Name:			Entity
 *	Author:			Jordan Woerner
 *	Description:	Provides a few basic attributes for any Entities that will 
					render in the game world.
 *****************************************************************************/

/* Headers from this project. */
#include "types/Shader.h"
#include "types/PhysicsStructs.h"
#include "types/Mesh.h"
/* External library headers. */
#include "glm/glm.hpp"

class Entity
{
public:

	/*
	 Constructs a new Entity.
	 */
	Entity() : mvMat(glm::mat4(1.0f)) {
		
		state.fwd = glm::vec3(0.0f, 0.0f, -1.0f);
		state.right = glm::vec3(-1.0f, 0.0f, 0.0f);
		state.up = glm::vec3(0.0f, 1.0f, 0.0f);
	}

	/*
	 Gets the position of this Entity.
	 */
	inline State getState() const { return state; }

	/*
	 Sets the Mesh object this Entity uses.
	 */
	inline virtual void setMesh(Mesh& m) { mesh = &m; }

	/*
	 Returns a reference to the Mesh object this Entity uses.
	 */
	inline Mesh& getMesh() const { return *mesh; }

	/*
	 Returns all the lights attatched to this Entity.
	 */
	const std::vector<Light*>& getLights() const { return lights; }

	/*
	 Initialises the Enttiy using the provided shader.
	 */
	virtual void init() = 0;
	
	/*
	 Updates the entity, interpolating changes with delta time.
	 */
	virtual void update(float dt, float timesscale = 1.0f) = 0;

	/*
	 Moves this Entity around.
	 */
	virtual void move(float dt) = 0;

	/*
	 Renders this Entity.
	 */
	virtual void render(const glm::mat4 &view, const glm::mat4 &proj, 
		const std::vector<Light*> &lights, const Fog &fog, 
		const float dt = 0.0f) = 0;

protected:
	State state;		// The current physical state.
	State prevState;	// The last physical state.

	glm::mat4 mvMat;	// The Model-View matrix for this Entity.
	Mesh *mesh;	// The Mesh this Entity uses.
	std::vector<Light*> lights; // Any lights that this Entity owns.
};

#endif // ENTITY_H