#ifndef U_DEBUG_CONSOLE_H
#define U_DEBUG_CONSOLE_H
/******************************************************************************
 *	Name:			DebugConsole
 *	Author:			Jordan Woerner
 *	Description:	DebugConsole allows the Game to print debugging details out
					 during runtime by redirecting the standard output pipes. 
					Requires _DEBUG to be defined for use.
 *****************************************************************************/

#define WIN32_LEAN_AND_MEAN

#include <fstream>
#include <fcntl.h>
#include <io.h>
#include <windows.h>

class DebugConsole
{
public:
	/*
	 DebugConsole()
	 Creates a default console with 500 rows and 80 columns.
	 */
	DebugConsole();
	/*
	 DebugConsole(int, int)
	 Creates a console with 'x' rows and 'y' columns.
	 */
	DebugConsole(int rows, int cols);

	/*
	 open()
	 Opens a console window. Redirecting standard output.
	 */
	void open();
	/*
	 close()
	 Closes a console window, removing the console.
	 */
	void close();

	/*
	 ~DebugConsole()
	 Removes the console.
	 */
	~DebugConsole();

private:
	const WORD MAX_ROWS;	// Maximum number of rows to keep in the buffer.
	const WORD MAX_COLS;	// Maximum number of characters per row.
	FILE* fp;				// The handle for input and output.
	HANDLE hStdHandle;		// The handle for I/O streams.
	int hConHandle;			// The handle for the console.
};

#endif // U_DEBUG_CONSOLE_H