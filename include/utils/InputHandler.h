#ifndef EVENT_HANDLER_H
#define EVENT_HANDLER_H
/******************************************************************************
 *	Name:			InputHandler
 *	Author:			Jordan Woerner
 *	Description:	InputHandler holds the state of various inputs from the 
					system, along with storing other input related things for 
					game logic. The class is intended for a singular use per 
					application, to then be shared across other objects 
					allowing for a centralised object to hold all input data.
 *****************************************************************************/

#include "../glm/glm.hpp"

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <Xinput.h>

/* Top-row number key aliases. */
#define KEY_0 0x30
#define KEY_1 0x31
#define KEY_2 0x32
#define KEY_3 0x33
#define KEY_4 0x34
#define KEY_5 0x35
#define KEY_6 0x36
#define KEY_7 0x37
#define KEY_8 0x38
#define KEY_9 0x39
/* Letter key aliases. */
#define KEY_A 0x41
#define KEY_B 0x42
#define KEY_C 0x43
#define KEY_D 0x44
#define KEY_E 0x45
#define KEY_F 0x46
#define KEY_G 0x47
#define KEY_H 0x48
#define KEY_I 0x49
#define KEY_J 0x4A
#define KEY_K 0x4B
#define KEY_L 0x4C
#define KEY_M 0x4D
#define KEY_N 0x4E
#define KEY_O 0x4F
#define KEY_P 0x50
#define KEY_Q 0x51
#define KEY_R 0x52
#define KEY_S 0x53
#define KEY_T 0x54
#define KEY_U 0x55
#define KEY_V 0x56
#define KEY_W 0x57
#define KEY_X 0x58
#define KEY_Y 0x59
#define KEY_Z 0x5A
/* Special key aliases. */
#define KEY_BACK	0x08 // Backspace.
#define KEY_TAB		0x09 // Tab key.
#define KEY_RET		0x0D // Return key.
#define KEY_SHFT	0x10 // Shift key.
#define KEY_LSHFT	0xA0 // Left shift key.
#define KEY_RSHFT	0xA1 // Right shift key.
#define KEY_CTRL	0x11 // Ctrl key.
#define KEY_LCTRL	0xA2 // Left ctrl key.
#define KEY_RCTRL	0xA3 // Right ctrl key.
#define KEY_ALT		0x12 // Alt key.
#define KEY_LALT	0xA4 // Left alt key.
#define KEY_RALT	0xA5 // Right alt key.
#define KEY_PAUS	0x13 // Pause.
#define KEY_SPC		0x20 // Spacebar.
#define KEY_PGUP	0x21 // Page up.
#define KEY_PGDN	0x22 // Page down.
#define KEY_END		0x23 // End key.
#define KEY_HOME	0x24 // Home key.
#define KEY_INS		0x2D // Insert key.
#define KEY_DEL		0x2E // Delete key.
#define KEY_NUML	0x90 // Numlock key.
#define KEY_SCRL	0x91 // Scoll lock key.
#define KEY_LWIN	0x5B // Left Windows key.
#define KEY_RWIN	0x5C // Right Windows key.
#define KEY_APPS	0x5D // Apps key.
/* Mouse button aliases. */
#define MOUSE_L 0x01 // Left mouse button.
#define MOUSE_R 0x02 // Right mouse button.
#define MOUSE_M 0x04 // Middle mouse button.
#define MOUSE_1 0x05 // Extra mouse button 1.
#define MOUSE_2 0x06 // Extra mouse button 2.
/* Arrow key aliases. */
#define KEY_LEFT	0x25 // Left arrow key.
#define KEY_UP		0x26 // Up arrow key.
#define KEY_RIGHT	0x27 // Right arrow key.
#define KEY_DOWN	0x28 // Down arrow key.
/* Numpad key aliases. */
#define KEY_NP_0 0x60
#define KEY_NP_1 0x61
#define KEY_NP_2 0x62
#define KEY_NP_3 0x63
#define KEY_NP_4 0x64
#define KEY_NP_5 0x65
#define KEY_NP_6 0x66
#define KEY_NP_7 0x67
#define KEY_NP_8 0x68
#define KEY_NP_9 0x69
/* Function key aliases. */
#define KEY_F1	0x70
#define KEY_F2	0x71
#define KEY_F3	0x72
#define KEY_F4	0x73
#define KEY_F5	0x74
#define KEY_F6	0x75
#define KEY_F7	0x76
#define KEY_F8	0x77
#define KEY_F9	0x78
#define KEY_F10 0x79
#define KEY_F11 0x7A
#define KEY_F12	0x7B
#define KEY_F13	0x7C

/* Gamepad button aliases. */
#define PAD_A 0x100	// Gamepad A button.
#define PAD_B 0x200	// Gamepad B button.
#define PAD_X 0x400	// Gamepad X button.
#define PAD_Y 0x800	// Gamepad Y button.
#define PAD_BACK	0x0020 // Gamepad Back button.
#define PAD_START	0x0010 // Gamepad Start/Pause button.
#define PAD_LB 0x0100 // Gamepad left bumper.
#define PAD_RB 0x0200 // Gamepad right bumper.
#define PAD_LS	0x0040 // Gamepad left stick click.
#define PAD_RS	0x0080 // Gamepad right stick click.
#define PAD_DPAD_UP		0x0001 // Gamepad D-Pad up.
#define PAD_DPAD_DOWN	0x0002 // Gamepad D-Pad down.
#define PAD_DPAD_LEFT	0x0004 // Gamepad D-Pad left.
#define PAD_DPAD_RIGHT	0x0008 // Gamepad D-Pad right.x

#define KEY_PRESS_DELAY 0.5f	// The delay between key repetions in seconds.

/*
 GameState
 An enumeration of all possible states the game can be in.
 */
enum GameState {

	MAIN_MENU,
	PLAYING,
	PAUSE
};

class InputHandler {

private:
	bool keys[256];		// An array of the states for 256 keys.
	unsigned long lastKeyPressTime[265]; // The last time this key was pressed.
	long mouseX;		// The X position of the mouse.
	long mouseY;		// The Y position of the mouse.
	bool mouseSet;		// Does the mouse position need setting?
	bool mouseLocked;	// Is the mouse locked to the GLDisplay?
	GameState state;	// The current game state.

	XINPUT_STATE controller; // The state of gamepad inputs.
	float stickDeadZone;	// The deadzone of the thumbsticks.
	float triggerDeadZone;	// The deadzone of the triggers.

public:
	/*
	 Creates a new InputHandler to manage the key presses and mouse movements 
	 between objects in the game.
	 */
	InputHandler();

	/*
	 Sets the specified key to either true or false depending on the state of 
	 the key.
	 */
	inline void setKey(unsigned char key, bool val) { 
		
		keys[key] = val;
		lastKeyPressTime[key] = GetTickCount();
	}

	/*
	 Returns the state of the specified key, true for down, false for up.
	 */
	inline bool getKey(unsigned char key) const { return keys[key]; }

	/*
	 Returns the last time the specified key was pressed.
	 */
	inline unsigned long getLastTimePressed(unsigned char key) { 

		return lastKeyPressTime[key]; 
	}

	/*
	 Sets the stored X position of the mouse cursor.
	 */
	inline void setMouseX(long x) { mouseX = x; }

	/*
	 Returns the X position of the mouse;
	 */
	inline long getMouseX() const { return mouseX; }

	/*
	 Sets the stored Y position of the mouse cursor.
	 */
	inline void setMouseY(long y) { mouseY = y; }

	/*
	 Returns the Y position of the mouse;
	 */
	inline long getMouseY() const { return mouseY; }

	/*
	 Is the mouse position set or does it need setting? True if set, false otherwise.
	 */
	inline bool isMousePosSet() const { return mouseSet; }

	/*
	 Set whether or not the mouse position needs setting or not. False makes mouse position
	 set next update.
	 */
	inline void setMousePosSet(bool set) { mouseSet = set; }

	/*
	 True stops mouse controls from working.
	 */
	inline void setMouseLocked(bool m) { mouseLocked = m; }

	/*
	 Returns true if mouse input is active. False otherwise.
	 */
	inline bool isMouseLocked() const { return mouseLocked; }

	/*
	 Sets the current GameState to the specified enumeration value.
	 */
	inline void setGameState(GameState s) { state = s; }

	/*
	 Gets the current state of the game as a GameState enumeration value.
	 */
	inline GameState getGameState() const { return state; }

	/*
	 Returns true if the player 1 gamepad is connected. False otherwise.
	 */
	bool isGamepadConnected();

	/*
	 Returns the position of the left stick (normalised).
	 */
	glm::vec2 getLeftStickPos() const;

	/*
	 Returns the position of the right stick (normalised).
	 */
	glm::vec2 getRightStickPos() const;

	/*
	 Gets the actuation of the left trigger (normalised).
	 */
	float getLeftTrigger() const;

	/*
	 Gets the actuation of the right trigger (normalised).
	 */
	float getRightTrigger() const;

	/*
	 Gets the state of a button on the gamepad. True if pressed, false otherwise.
	 */
	bool getGamepadButton(unsigned short button);

	/*
	 Returns the dead zone of the thumbsticks.
	 */
	inline float getStickDeadZone() const { return stickDeadZone; }
	
	/*
	 Returns the dead zone of the triggers.
	 */
	inline float getTriggerDeadZone() const { return triggerDeadZone; }

	/*
	 Resets this InputHandler.
	 */
	inline void reset() {

		for (int i = 0; i < 256; i++) {
			InputHandler::keys[i] = false;
			InputHandler::lastKeyPressTime[i] = 0L;
		}
		InputHandler::mouseX = 0;
		InputHandler::mouseY = 0;
		ZeroMemory(&controller, sizeof(XINPUT_STATE));
	}

};

#endif // EVENT_HANDLER_H