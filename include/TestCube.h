#pragma once

#include "Collidable.h"
#include "types/Entity.h"

class TestCube : public Entity, public Collidable
{
public:
	TestCube();
	TestCube(int n);

	void init();

	void update(float dt, float timescale = 1.0f);

	void move(float dt) {}

	void resolveCollision(std::vector<glm::vec3> &shape, std::vector<glm::vec3> &simplex);

	void render(const glm::mat4 &view, const glm::mat4 &proj, const std::vector<Light*> &lights, const Fog &fog, const float dt = 0.0f);

	~TestCube();
	
	glm::vec3 scale;

};

