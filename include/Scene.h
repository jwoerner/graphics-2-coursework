#ifndef SCENE_H
#define SCENE_H
/******************************************************************************
 *	Name:			Scene
 *	Author:			Jordan Woerner
 *	Description:	Contains all the Objects needed for the game.
 *****************************************************************************/

#include "Camera.h"
#include "Floor.h"
#include "Player.h"
#include "Skybox.h"
#include "types/Entity.h"
#include "types/Mesh.h"
#include "types/Texture.h"
#include "utils/InputHandler.h"

#include <vector>

class Scene
{
public:

	/* 
	 Creates a new Scene with the provided InputHandler attatched.
	 */
	Scene(InputHandler *e, Camera &c);

	/*
	 Sets up this Scene and anything inside it,
	 */
	void init();

	/*
	 Updates all the things in the Scene.
	 */
	void update(float dt, float timeScale = 1.0f);

	/*
	 Renders everything in the Scene.
	 */
	void render(float interp = 1.0f);

	/*
	 Sets the Camera to be used by the Player in this Scene.
	 */
	inline void setCam(Camera& c) { cam = c; }

	/*
	 Sets the projection matrix to pass to shaders.
	 */
	inline void setProjection(glm::mat4 p) { proj = p; }

	/*
	 Loads a specified model into the Scene.
	 */
	void loadModel(std::string file, std::string path);

	/*
	 Safely cleans up the resources in use by this Scene.
	 */
	~Scene();

private:
	InputHandler* ev;	// InputHandler for the Scene.
	Camera &cam;		// Camera for the Scene.
	glm::mat4 proj;		// Projection matrix for the renderer.
	Player *ply;		// The player entity.
	Skybox sky;			// Skybox for the Scene.
	Floor floor;		// The world to drive around.
	Fog fog;			// Fog for the world.

	std::vector<Entity*> ents; // Vector of Entities in the Scene.
	std::vector<Entity*>::iterator entsIt; // Iterator for the Entities.
	std::vector<Mesh*> meshes; // Vector of Meshes in the Scene.
	std::vector<Mesh*>::iterator meshIt; // Iterator for the Meshes.
	std::vector<Shader*> shaders; // Vector of Shaders used for rendering.
	std::vector<Shader*>::iterator shdIt; // Iterator for the Shaders.
	std::vector<Light*> lights; // Vector of Lights in the Scene.
	std::vector<Light*>::iterator lightIt; // Iterator for the Lights.
	std::vector<Texture*> tex; // Vector of Textures in the Scene.
	std::vector<Texture*>::iterator texIt; // Iterator for the Textures.
};

#endif // SCENE_H