#ifndef PLAYER_H
#define PLAYER_H
/******************************************************************************
 *	Name:			Player
 *	Author:			Jordan Woerner
 *	Description:	Stores details about the user controlled Player, such as 
					model in use and position/ rotation attributes.
 *****************************************************************************/

/* Headers from this project. */
#include "Collidable.h"
#include "types/Entity.h"
#include "types/Texture.h"
#include "utils/InputHandler.h"

class Player : public Entity, public Collidable
{
public:
	/*
	 Creates a new Player at the origin.
	 */
	Player(InputHandler& in);

	/*
	 Set up anything specific to the Player.
	 */
	void init();

	/*
	 Sets the distance this Player can go before teleporting around.
	 */
	inline void setWorldLimit(glm::vec2 size) { worldLimit = size; }

	/*
	 Updates the Player.
	 */
	void update(float dt, float timesscale = 1.0f);

	/*
	 Moves the Player.
	 */
	void move(float dt);

	/*
	 Resolves the collision with the specified shape.
	 */
	void resolveCollision(Collidable* other, std::vector<glm::vec3> &simplex);

	/*
	 Draws the Player to the screen.
	 */
	void render(const glm::mat4 &view, const glm::mat4 &proj, const std::vector<Light*> &lights, const Fog &fog, const float dt = 0.0f);

	/*
	 Removes the Player safely.
	 */
	~Player();

private:
	InputHandler &input;

	const float maxSpeed;	// The maximum velocity the Player can achieve.
	float accelIn;
	float accelFact;
	float steerIn;
	float steerFact;
	float steerLerp;
	float latFricFact;
	float backFricFact;

	glm::vec2 worldLimit;
};

#endif // PLAYER_H