#ifndef LIGHT_SPOT_PARENTED_H
#define LIGHT_SPOT_PARENTED_H
/******************************************************************************
 *	Name:			ParentedSpot
 *	Author:			Jordan Woerner
 *	Description:	A specialisation of the Spotlight class that allows for it 
					to be attatched to an Entity and follow the Entity position
					 and rotation.
 *****************************************************************************/

#include "types/Entity.h"
#include "types/Spotlight.h"

class ParentedSpot : public Spotlight
{
public:
	/*
	 Creates a Spotlight that follows the specified Entity.
	 */
	ParentedSpot(Entity &parent);

	/*
	 Creates a Spotlight that follows the specified Entity. Position is and 
	 offset from (0, 0, 0) for movement. Direction will rotate with owner.
	 */
	ParentedSpot(Entity &parent, glm::vec3 pos, glm::vec3 dOff, float spotSize, 
		float multiplier);

	/*
	 Creates a Spotlight that follows the specified Entity. Position is and 
	 offset from (0, 0, 0) for movement. Direction will rotate with owner.
	 */
	ParentedSpot(Entity &parent, glm::vec3 pos, glm::vec3 col, glm::vec3 spec, 
		glm::vec3 dOff, float spotSize, float multiplier, float radius = 1.0f);

	/*
	 Updates this ParentedSpot.
	 */
	void update(float dt, float timeScale = 1.0f);

private:
	Entity &owner;			// The Entity this ParentedSpot should follow.
	glm::vec3 offset;		// The starting position for this ParentedSpot.
	glm::vec3 dirOffset;	// Directional offset for this ParentedSpot.

};

#endif // LIGHT_SPOT_PARENTED_H