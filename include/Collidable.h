#ifndef COLLIDABLE_H
#define COLLIDABLE_H
/******************************************************************************
 *	Name:			Collidable
 *	Author:			Jordan Woerner
 *	Description:	Provides methods for testing for collisions in the game 
					world. Must be implmeneted in an Entity (or derivative) 
					that wants to be pysical.
 *****************************************************************************/

#include "PhysicsSystem.h"
#include "types/PhysicsStructs.h"
#include "types/Shader.h"

#include "glm/glm.hpp"
#include "glm/gtc/constants.hpp"
#include "glm/gtc/matrix_transform.hpp"

#include <fstream>
#include <sstream>
#include <vector>

class Collidable
{
public:
	Collidable() : minXYZ(glm::vec3(std::numeric_limits<float>::max())), radius(0.0f) {}

	/*
	 Gets the minimum X, Y and Z points of this Collidable.
	 */
	inline glm::vec3 getMinXYZ() const { return minXYZ; }
	
	/*
	 Gets the maximum X, Y and Z points of this Collidable.
	 */
	inline glm::vec3 getMaxXYZ() const { return maxXYZ; }

	/*
	 Gets the center point of this Collidable.
	 */
	inline glm::vec3 getCenter() const { return center; }

	/*
	 Gets the radius of the bounding sphere of this Collidable.
	 */
	inline float getRadius() const { return radius; }

	/*
	 Checks for intersections between this Collidable and all the Collidables 
	 in the provided vector.
	 */
	virtual void checkCollisions(std::vector<Collidable*> &other) {
		
		std::vector<Collidable*>::iterator it;
		std::vector<glm::vec3> simplex;
		for (it = other.begin(); it != other.end(); it++) {
			if (this != (*it)) { // Avoid checking against self.
				/* Perform a bounding sphere test. */
				if (!PhysicsSystem::boundSphereIntersection(
					this->center, (*it)->center,
					this->radius, (*it)->radius)
					) {	continue; } // No intersection.
				/* If there is an possble intersection, use GJK to check it properly. */
				simplex.clear();
				if (PhysicsSystem::isIntersecting(this->collisionMesh, (*it)->collisionMesh, simplex)) {
					//std::fprintf(stdout, "Narrow\n");
					resolveCollision((*it), simplex);
				}
			}
		}
	}

	/*
	 Handles the collision between this Collidable and another.
	 */
	virtual void resolveCollision(Collidable* other, std::vector<glm::vec3> &simplex) {}

	/*
	 Loads the collision mesh stored in the specified file. Must be OBJ format.
	 */
	bool loadCollisionMesh(const std::string path) {

		collisionMesh.clear();
		std::ifstream file(path);
		if (!file) {
			std::fprintf(stderr, "Could not open %s!\n", file);
			return false;
		}

		std::string line;
		while (getline(file, line)) {
			if (line.substr(0, 2) == "v ") {
				std::istringstream parts(line.substr(2));
				glm::vec3 vert;
				parts >> vert.x; parts >> vert.y; parts >> vert.z;
				createBoundingBox(vert);
				initialPoints.push_back(vert);
			}
		}
		collisionMesh.resize(initialPoints.size());

		return true;
	}

	/*
	 Sets the collision mesh for this Collidable to the provided vector of 
	 float. Converts values into glm::vec3.
	 */
	void setCollisionMesh(std::vector<float> mesh) {

		collisionMesh.clear();
		for (unsigned int i = 0; i < mesh.size(); i+=3) {
			glm::vec3 vert(mesh[i], mesh[i + 1], mesh[i + 2]);
			createBoundingBox(vert);
			initialPoints.push_back(vert);
		}
		collisionMesh.resize(initialPoints.size());
	}

	/*
	 Sets the collision mesh for this Collidable to the provided vector of 
	 glm::vec3.
	 */
	inline void setCollisionMesh(std::vector<glm::vec3> mesh) { 
		
		for (unsigned int i = 0; i < mesh.size(); i++) {
			createBoundingBox(mesh.at(i));
		}
		initialPoints = mesh; 
		collisionMesh.resize(initialPoints.size());
	}

	/*
	 Returns a reference to the convex collision mesh for this Collidable.
	 */
	inline const std::vector<glm::vec3>& getCollisionMesh() const { return collisionMesh; }

	/*
	 Updates the points of the collision mesh in use by this Collidable.
	 */
	void updateCollisionMesh(State &s) {
		
		glm::mat4 rotMat = glm::rotate(glm::mat4(1.0f), s.rot.x, glm::vec3(1.0f, 0.0f, 0.0f));
		rotMat = glm::rotate(rotMat, s.rot.y, glm::vec3(0.0f, 1.0f, 0.0f));
		rotMat = glm::rotate(rotMat, s.rot.z, glm::vec3(0.0f, 0.0f, 1.0f));

		for (unsigned int i = 0; i < collisionMesh.size(); i++) {
			glm::vec4 newPos = glm::vec4(initialPoints.at(i), 1.0f) * rotMat;
			collisionMesh.at(i).x = newPos.x + s.pos.x;
			collisionMesh.at(i).y = newPos.y + s.pos.y;
			collisionMesh.at(i).z = newPos.z + s.pos.z;
			/* Once the vertex is up to date, update the bounding box. */
			updateBoundingBox(s.pos);
		}
	}

	void drawCollisionMesh(const glm::mat4 &proj, const glm::mat4 &view) {

		GLuint program = shader.getHandle();
		glUseProgram(program);

		/* Get the position of a few common matricies. */
		GLint projLoc = shader.getUniform("projection");
		GLint viewLoc = shader.getUniform("view");
		GLint mvMatLoc = shader.getUniform("model");

		glUniformMatrix4fv(projLoc, 1, GL_FALSE, &proj[0][0]);
		glUniformMatrix4fv(viewLoc, 1, GL_FALSE, &view[0][0]);
		glUniformMatrix4fv(mvMatLoc, 1, GL_FALSE, &modelView[0][0]);

		glBindVertexArray(vao);
		glDrawArrays(GL_LINE_LOOP, 0, collisionMesh.size() + 9);
	}

protected:
	std::vector<glm::vec3> collisionMesh; // The points that make up the convex collision bounds.
	std::vector<glm::vec3> initialPoints; // The points without any transformations.

	glm::vec3 minXYZ, maxXYZ, center;	// Points for the bounding box of this Collidable.
	glm::vec3 initMax, initMin, initCen; // Initial bounding box points.
	float radius;	// The radius of the enclosing sphere.

private:
	GLuint *vbo;
	GLuint vao;
	Shader shader;
	glm::mat4 modelView;

	/*
	 Updates the limits of the bounding box and bounding sphere.
	 */
	void createBoundingBox(glm::vec3 testVec) {

		/* Update the maximum point of the bounding box. */
		if (testVec.x > maxXYZ.x) maxXYZ.x = testVec.x;
		if (testVec.y > maxXYZ.y) maxXYZ.y = testVec.y;
		if (testVec.z > maxXYZ.z) maxXYZ.z = testVec.z;
		/* Update the minim point of the bounding box. */
		if (testVec.x < minXYZ.x) minXYZ.x = testVec.x;
		if (testVec.y < minXYZ.y) minXYZ.y = testVec.y;
		if (testVec.z < minXYZ.z) minXYZ.z = testVec.z;
		/* Move the center point relevant to the new bounding box. */
		center = (maxXYZ + minXYZ) / 2.0f;
		/* Update the radius of the bounding sphere.*/
		if (fabs(testVec.x) > radius) radius = abs(testVec.x);
		if (fabs(testVec.y) > radius) radius = abs(testVec.y);
		if (fabs(testVec.z) > radius) radius = abs(testVec.z);

		initMax = maxXYZ;
		initMin = minXYZ;
		initCen = center;
	}

	void setupGL() {

		if (shader.getHandle() == 0) {
			if (!shader.load("Debug", "resource/shaders/debug.vert", "resource/shaders/debug.frag")) {
				return;
			}
		}

		GLint program = shader.getHandle();
		glUseProgram(program);

		/* Get the locations of required shader attributes. */
		GLint posAttrib = shader.getAttrib("v_pos");

		glGenVertexArrays(1, &vao);
		glBindVertexArray(vao);

		vbo = new GLuint[4];
		glGenBuffers(4, vbo);

		glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);
		glBufferData(GL_ARRAY_BUFFER, collisionMesh.size() * sizeof(GLfloat), &collisionMesh, GL_STATIC_DRAW);
		glEnableVertexAttribArray(posAttrib);
		glVertexAttribPointer(posAttrib, 3, GL_FLOAT, GL_FALSE, 0, 0);

		glBindBuffer(GL_ARRAY_BUFFER, vbo[1]);
		glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3), &center[0], GL_STATIC_DRAW);
		glEnableVertexAttribArray(posAttrib);
		glVertexAttribPointer(posAttrib, 3, GL_FLOAT, GL_FALSE, 0, 0);

		glBindBuffer(GL_ARRAY_BUFFER, vbo[2]);
		float arr[8*3] = {
			minXYZ.x, minXYZ.y, minXYZ.z, 
			minXYZ.x, maxXYZ.y, minXYZ.z,
			minXYZ.x, minXYZ.y, maxXYZ.z,
			maxXYZ.x, minXYZ.y, minXYZ.z,
			maxXYZ.x, maxXYZ.y, minXYZ.z, 
			minXYZ.x, maxXYZ.y, maxXYZ.z, 
			maxXYZ.x, minXYZ.y, maxXYZ.z, 
			maxXYZ.x, maxXYZ.y, maxXYZ.z,
		};
		glBufferData(GL_ARRAY_BUFFER, 8 * 3 * sizeof(GLfloat), arr, GL_STATIC_DRAW);
		glEnableVertexAttribArray(posAttrib);
		glVertexAttribPointer(posAttrib, 3, GL_FLOAT, GL_FALSE, 0, 0);

		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindVertexArray(0);
		glUseProgram(0);
	}

	void updateBoundingBox(glm::vec3 pos) {

		maxXYZ = (initMax + pos);
		minXYZ = (initMin + pos);
		center = (initCen + pos);
	}
};

#endif // COLLIDABLE_H