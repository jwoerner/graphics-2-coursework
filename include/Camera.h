#ifndef CAMERA_H
#define CAMERA_H
/******************************************************************************
 *	Name:			Camera
 *	Author:			Jordan Woerner
 *	Description:	A Camera is the users view of the game world. Can be 
					controlled manually, attatched to an Entity, and 
					positioned based on the attributes of the Entity.
 *****************************************************************************/

/* Headers from this project. */
#include "types/Entity.h"
#include "utils/InputHandler.h"

class Camera
{
public:
	/*
	 Creates a new Camera at the origin facing up on Y.
	 */
	Camera(InputHandler &in);

	/*
	 Copy constructor for a Camera. Creates a deep copy of the provided Camera.
	 */
	Camera(Camera &other);

	/*
	 Sets the horizontal field of view for this camera in degrees. Converts to 
	 vertical internally.
	 */
	void setFOV(unsigned int w, unsigned int h, float f);

	/*
	 Set if the camera uses the position as an offset or an absolute.
	 */
	inline void setOrbit(bool enabled) { orbit = enabled; }

	/*
	 Sets the Entity the Camera tracks.
	 */
	inline void setTarget(const Entity& e) { target = &e; }

	/*
	 Gets the horizontal field of view for this Camera in degrees.
	 */
	inline float getVFOV() { return fov; }

	/*
	 Gets the vertical field of view for this Camera in degrees.
	 */
	float getHFOV();

	/*
	 Returns true if the Camera is using its position as an offset. False 
	 otherwise.
	 */
	inline bool isOrbiting() const { return orbit; }

	/* 
	 Gets the position of this Camera.
	*/
	const glm::vec3 getPos() const;

	/*
	 Gets the position the Camera is looking at.
	 */
	inline const glm::vec3& getAimPos() const { return aimPos; }

	/*
	 Gets the rotation of this Camera.
	 */
	inline const glm::vec3 getRot() const { return state.rot; }

	/*
	 Gets the view matrix from the Camera.
	 */
	const glm::mat4 getView();

	/*
	 Gets a reference to the Entity this Camera is targeting.
	 */
	inline const Entity& getTarget() const { return *target; }

	/*
	 Updates the Camera.
	 */
	void update(float timesscale = 1.0f);

	/*
	 Removes the Camera safely.
	 */
	~Camera();

	/*
	 Assigns this Camera the deep copy values of the provided Camera.
	 */
	Camera& operator=(const Camera& other);

private:
	unsigned int width;	// The width of the display this camera is using.
	unsigned int height;// The height of the display this camera is using.
	float fov;			// The vertical field of view for this camera.
	unsigned short camMode;	// The mode of the camera.

	bool orbit;			// Does the Camera orbint the target?
	float speed;		// The movement speed of this Camera.
	State state;		// The physics State of this Camera.
	glm::vec3 aimPos;	// The posistion to look at.
	glm::vec3 offset;	// The camera offset from the target for orbiting.

	const Entity* target;	// The Enity to target if specified.
	InputHandler &ev;	// InputHandler for mouse input.
	long lastMouse[2];	// The last mouse position.
	float sensitivity;	// Mouse sensitivity.

	unsigned long lastPressed; // When was a gamepad button last pressed.

	/*
	 Change the mode of the Camera. Switches between orbiting, first person 
	 and free control.
	 */
	void changeMode();
};

#endif // CAMERA_H