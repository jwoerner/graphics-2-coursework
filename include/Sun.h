#ifndef LIGHT_SUN_H
#define LIGHT_SUN_H
/******************************************************************************
 *	Name:			Sun
 *	Author:			Jordan Woerner
 *	Description:	A special implementation of Light to allow for day/ night 
					cycles, constant movement and rendering of a sun sprite.
 *****************************************************************************/

#include "types/Light.h"

class Sun : public Light
{
public:
	/*
	 Creates a new Sun.
	 */
	Sun(float t = 0.0f);

	/*
	 Creates a new Sun with the specified day and night colours.
	 */
	Sun(glm::vec3 dCol, glm::vec3 nCol, glm::vec2 axis, float t = 0.0f);

	/*
	 Updates the position and ambient colour of the Sun.
	 */
	void update(float dt, float timeScale = 1.0f);

private:
	glm::vec2 axis;		// The axis the Sun rotates on.
	glm::vec3 dayCol;	// The ambient colour for midday.
	glm::vec3 nightCol; // The ambient colour for midnight.

	glm::vec3 curCol;	// The current ambient colour of the Sun.
	float time;			// The current time of the Sun.
	bool day;			// Is it day or night time?

};

#endif // LIGHT_SUN_H