#ifndef PHYSICS_SYSTEM_H
#define PHYSICS_SYSTEM_H
/******************************************************************************
 *	Name:			PhysicsSystem
 *	Author:			Jordan Woerner
 *	Description:	Provides methods for updating the physical state of things,
					 along with testing for intersections.
 *****************************************************************************/

#include "types/PhysicsStructs.h"

#include "glm/glm.hpp"

#include <vector>

#define MAX_ATTEMPTS 20

class PhysicsSystem
{
public:
	/*
	 Checks for bounding sphere intersections.
	 */
	static bool boundSphereIntersection(glm::vec3 c1, glm::vec3 c2, float r1, float r2);

	/*
	 Performs a intersection test on two convex shapes using the GJK collision 
	 detection algorithm.
	 */
	static bool isIntersecting(std::vector<glm::vec3> &shape1, 
		std::vector<glm::vec3> &shape2, std::vector<glm::vec3> &simplex);

	/*
	 Gets the intersection direction and magnitude using the EPA algorithm.
	 */
	static glm::vec3 getIntersection(std::vector<glm::vec3> &shape1, 
		std::vector<glm::vec3> &shape2, std::vector<glm::vec3> &simplex);

	/*
	 Gets the Minimal Translation Vector (MTV) needed to push an object out of 
	 a collision.
	 */
	static glm::vec3 getMTV(std::vector<glm::vec3> &simplex);

private:
	/*
	 Calculates a glm::vec3 to use for the GJK collision detection algorithm. 
	 Returns true if the Origin is inside a glm::vec3.
	 */
	static bool getSimplex(std::vector<glm::vec3> &simplex, glm::vec3 &dir);

	/*
	 Calculates the max point in a specified convex shape in a specified 
	 direction.
	 */
	static glm::vec3 maxPoint(const std::vector<glm::vec3> &shape, glm::vec3 dir);

	/*
	 Calculates the max point of the Minowski difference of two shapes in 
	 a specified direction.
	 */
	static glm::vec3 maxPoint(const std::vector<glm::vec3> &shape1, 
		const std::vector<glm::vec3> &shape2, glm::vec3 dir);

	/*
	 Checks if a vector is on the same side of the Origin as a provided direction.
	 */
	static bool sameDirection(glm::vec3 &a, glm::vec3 &dir) {

		return (glm::dot(a, dir) > 0.0f);
	}

	/*
	 Looks for the closest face to the Origin in a Simplex.
	 */
	static Face closestFace(std::vector<glm::vec3> &simplex);
};

#endif // PHYSICS_SYSTEM_H