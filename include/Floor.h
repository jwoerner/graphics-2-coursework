#ifndef FLOOR_H
#define FLOOR_H
/******************************************************************************
 *	Name:			Floor
 *	Author:			Jordan Woerner
 *	Description:	Creates a surface that draws two textures based on a 
					provided surface description.
 *****************************************************************************/

#include "types/Plane.h"
#include "types/Texture.h"

class Floor : public Plane
{
public:
	/*
	 Creates a new Floor of size 1.0 with a world of the specified size.
	 */
	Floor(glm::vec2 worldSize);

	/*
	 Creates a new Floor of the specified size, in a world of the specified 
	 size.
	 */
	Floor(glm::vec2 worldSize, glm::vec2 size);

	/*
	 Sets this Floor up for rendering.
	 */
	void setup(Shader& shad, std::vector<Texture*> &textures, 
		std::string texture);

	/*
	 Renders this Floor to the screen.
	 */
	void render(const glm::mat4 &view, const glm::mat4 &proj, 
		const std::vector<Light*> &lights, const Fog &fog, 
		const float dt = 1.0f);

	~Floor();

private:
	glm::vec2 size;	// The size of the world this floor is in.
	GLint grassTex, grassTex_s;	// The grass textures.
	GLint roadTex, roadTex_s;	// The road textures.
	GLint roadMap;	// The texture that defines the layout.

};

#endif FLOOR_H