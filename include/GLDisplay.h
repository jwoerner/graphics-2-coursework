#ifndef GLDISPLAY_H
#define GLDISPLAY_H
/******************************************************************************
 *	Name:			GLDisplay
 *	Author:			Jordan Woerner
 *	Description:	GLDisplay is a wrapper for a Win32 window that contains an 
					OpenGL context. The Window can be linked to an 
					"InputHandler" to allow for any Win32 input to be shared
					with other components of the program with ease.
 *****************************************************************************/

#include "gl/glew.h"
#include "gl/wglew.h"

#include <Windows.h>

/* I'm fed up of hunting down the OGL attributes. */
#define OGL_VER_MAJ 3 // Set major OGL version.
#define OGL_VER_MIN 1 // Set minor OGL version.

#define OGL_CON_FWD WGL_CONTEXT_FORWARD_COMPATIBLE_BIT_ARB // Forward compatible context.
#define OGL_CON_DBG WGL_CONTEXT_DEBUG_BIT_ARB // Debug context.
#define OGL_CON_TYPE 0 // Selected OpenGL context type.

int main(); // Just forward declare this to stop errors.
 
class GLDisplay {

private:
	unsigned int width;		// The width of the window in pixels.
	unsigned int height;	// The height of the window in pixels.
	bool vSync;				// Vertical sync setting.
	bool fullScreen;		// Full screen setting of the window.
	char* title;			// The window title.
	const char* className;	// Unique ID for the window.
	bool lockMouse;			// Locks the mouse to the window.

	HINSTANCE hInstance;	// The handle for this instance, from Windows.
	HWND hWnd;	// A handle to this window provided by Windows.
	HDC hDC;	// The display context for this window created by Windows.
	HGLRC hRC;	// The rendering context for this window created by OpenGL.

	/*
	 The real Win32 message handler callback. Called by the false one when a 
	 window message requires it.
	 */
	LRESULT CALLBACK wndProc(HWND hWnd, UINT msg, WPARAM wPar, LPARAM lPar);

public:
	/*
	 Default constructor for an GLDisplay. Accepts the instance handle for this 
	 application as a parameter.
	 */
	GLDisplay();

	/*
	 Alternate constructor for a GLDisplay. Accepts the instance handle as with 
	 the default constructor, but also accepts width and height values 
	 (in pixels) for the size of the window.
	 */
	GLDisplay(unsigned int w, unsigned int h);
	
	/*
	 Returns true if the display is using vertical sync, false otherwise.
	 */
	inline bool isVSync() { return vSync; }

	/*
	 Returns true if the display is fullscreen. False otherwise.
	 */
	inline bool isFullScreen() { return fullScreen; }
	
	/*
	 Returns the width of this window in pixels as an integer.
	 */
	inline unsigned int getWidth() const { return width; }

	/*
	 Sets the desired width of this window in pixels to the provided unsigned 
	 integer.
	 */
	inline void setWidth(unsigned int w) { width = w; }

	/*
	 returns the height of this window in pixels as an integer.
	 */
	inline unsigned int getHeight() const { return height; }

	/*
	 Sets the desired height of this window in pixels to the provided unsigned 
	 integer.
	 */
	inline void setHeight(unsigned int h) { height = h; }

	/*
	 Gets the aspect ratio of the display.
	 */
	inline float getAspect() const { return (float)(width/height); }

	/*
	 Returns true if the mouse is locked to the window.
	 */
	inline bool isMouseLocked() const { return lockMouse; }

	/*
	 Turns vertical synchronisation of the display on or off.
	 */
	void setVSync(bool toggle);

	/*
	 Enables or disables fullscreen.
	 */
	inline void setFullscreen(bool toggle) { fullScreen = toggle; }
	 
	/*
	 Returns the Device Context for this window.
	 */
	inline HDC getDC() const { return hDC; }

	/*
	 Gets the mouse position and stores it in the provided array of long.
	 */
	void getMousePos(long *pos) const;

	/*
	 Sets the mouse position to the specified X and Y values.
	 */
	void setMousePos(long x, long y) const;

	/*
	 False message handler that decides how to deal with a message passed to 
	 this window properly.
	 */
	static LRESULT CALLBACK stWndProc(HWND hWnd, UINT msg, WPARAM wPar, LPARAM lPar);

	/*
	 Creates a Win32 window and a valid OpenGL context inside that window. Sets 
	 the title of the window to the provided TCHAR* array.
	 Returns true if the window is created successfully.
	 */
	bool createGLWindow(char* title);
	
	/*
	 Resizes the display safely, and fixes the OpenGL context to match the 
	 resized display.
	 */
	void resize(int w, int h);

	/*
	 Sets this GLDisplay to fullscreen.
	 */
	bool makeFullscreen();
	
	/*
	 Sets this GLDisplay to windowed mode.
	 */
	bool makeWindowed();

	/*
	 Clears the GLDisplay allowing for updates.
	 */
	void update();

	/*
	 Gets the current system time in ms.
	 */
	static float time();

	/*
	 Safely removes this GLWindow by destroying the OpenGL context, and then 
	 the window itself.
	 */
	void killGLWindow();

	/*
	 Runs any needed cleanup when destroying this Window.
	 */
	~GLDisplay();
};

/*
 Main entry point for Windows.
 */
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE pInstance, 
	LPSTR lpCmdLine, int nCmdShow);

#endif // GLDISPLAY_H