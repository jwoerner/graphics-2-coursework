#ifndef SKYBOX_H
#define SKYBOX_H
/******************************************************************************
 *	Name:			Skybox
 *	Author:			Jordan Woerner
 *	Description:	Draws a box around the specified Camera that follows it.
					Renders before everything else, ignoring the depth-buffer.
 *****************************************************************************/

/* Headers from this project. */
#include "Camera.h"
#include "Drawable.h"

#include <string>

class Skybox : public Drawable
{
public:
	/*
	 Creates a new Skybox.
	 */
	Skybox(Camera& c);

	/*
	 Returns true if its daytime, false otherwise.
	 */
	inline bool isDay() const { return day; }

	/*
	 Sets up the OpenGL resources for this Skybox.
	 */
	void setup(Shader& shad, std::vector<Texture*> &textures, std::string texture);

	/*
	 Updates the skybox.
	 */
	void update(float dt, float timescale = 1.0f);

	/*
	 Draws the Skybox.
	 */
	void render(const glm::mat4& view, const glm::mat4& proj, std::vector<Light*>& lights, const Fog &fog);

	/*
	 Cleans up the Skybox.
	 */
	~Skybox();

private:
	Camera& cam;	// Camera to adjust this Skybox by.
	GLuint skyTex;	// Day texture for the Skybox sky.
	GLuint skyTexN;	// Night texture for the Skybox sky.
	float time;		// The time that has passed in game.
	bool day;		// Is it day time?
};

#endif // SKYBOX_H