#version 140

struct Light {
	vec4 pos;		// Position of the light.
	vec3 spotDir;	// Direction the spotlight is pointing.
	vec3 diffuse;	// Diffuse relfection of the light.
	vec3 specular;	// Specular reflection of the light.
	float spotSize;	// Size the spotlight emits full light to.
	float spotCutoff; // Size the spotlight emits zero light at.
	float spotMult;	// Power of the spotlight.
	bool enabled;	// Is the light enabled or not?
	/* Attenuation variables. */
	float constAttenuation, linearAttenuation, quadAttenuation;
};

struct Fog {	
	vec4 colour;	// The colour of the fog.
	float start;	// The distance the fog begins.
	float end;		// The distacne the fog ends.
	float density;	// The density of the fog.
	int type;		// The equation for calculating fog density (0 = linear, 1 = exp, 2 = exp2)
};

struct Material {
	vec4 ambient;	// Ambient colour of the material.
	vec4 diffuse;	// Diffuse reflection component.
	vec3 spec;		// Specular reflection component.
	float shine;	// Reflection sharpness
};

const int lightCount = 15;	// The light limit (performance reasons).

uniform Light lights[lightCount]; // The lights passed to this shader.
uniform Material m;	// Material parameters.
uniform Fog f;	// Fog parameters.

in vec3 f_pos;
in vec3 f_nor;
in vec4 f_eye;

out vec4 o_colour;

/*
 Calculates the fog for this fragment.
 */
float calcFogFactor(Fog fog, float fogPos) {
	
	float result = 0.0;

	if (fog.type == 0) {
		// Linear fog.
		result = (f.end - fogPos)/(fog.end - fog.start);
	} else if (fog.type == 1) {
		// Exp fog.
		result = exp(-fog.density * fogPos);
	} else if (fog.type == 2) {
		// Exp2 fog.
		result = exp(-pow(fog.density * fogPos, 2.0));
	}

	result = 1.0 - clamp(result, 0.0, 1.0);
	return result;
}

void main()
{
	float fogDist = abs(f_eye.z / f_eye.w);
	if (f.type == 0 && fogDist > f.end) 
		discard; // Exit this frag calculation  early if we can.

	vec3 surfaceColour = vec3(0.0, 0.0, 0.0);	// Replace with texture lookup
	vec3 lightDir = vec3(0.0, 0.0, 0.0);
	float atten = 0.0f;

	for (int i = 0; i < lightCount; i++) {
		if (!lights[i].enabled) continue; // Skip this light if it's disabled.
		vec3 lightToVert = (lights[i].pos.xyz - f_pos);
		float distance = length(lightToVert);
		atten = mix(1.0, 
				1.0 / (lights[i].constAttenuation
					+ (lights[i].linearAttenuation * distance)
					+ (lights[i].quadAttenuation * distance * distance)),
				lights[i].pos.w);
		lightDir = normalize(lightToVert / distance);
	
		// Check if the light is a spotlight next.
		if (lights[i].spotSize <= 90.0) {
			// Spotlight
			float cosine = max(0.0, dot(-lightDir, lights[i].spotDir));
			if (cosine < cos(radians(lights[i].spotSize))) {
				// Fragment outside of spotlight influence.
				atten = 0.0;
			} else {
				atten = atten * pow(cosine, lights[i].spotMult);
			}
		}

		float diffIntensity = max(dot(f_nor, lightDir), 0.0);
		vec3 diffReflection = atten * lights[i].diffuse * m.diffuse.rgb * diffIntensity;

		surfaceColour += diffReflection;
	}

	o_colour = vec4(surfaceColour, m.ambient.a);
	o_colour = mix(o_colour, f.colour, calcFogFactor(f, fogDist));
}