#version 140

struct Fog {	
	vec4 colour;	// The colour of the fog.
	float start;	// The distance the fog begins.
	float end;		// The distacne the fog ends.
	float density;	// The density of the fog.
	int type;		// The equation for calculating fog density (0 = linear, 1 = exp, 2 = exp2)
};

in vec2 f_tex;
in vec4 f_eye;

uniform sampler2D day;
uniform sampler2D night;
uniform Fog f;
uniform float time;
uniform bool isDay;

out vec4 o_colour;

/*
 Calculates the fog for this fragment.
 */
float calcFogFactor(Fog fog, float fogPos) {
	
	float result = 0.0;

	if (fog.type == 0) {
		// Linear fog.
		result = (f.end - fogPos)/(fog.end - fog.start);
	} else if (fog.type == 1) {
		// Exp fog.
		result = exp(-fog.density * fogPos);
	} else if (fog.type == 2) {
		// Exp2 fog.
		result = exp(-pow(fog.density * fogPos, 2.0));
	}

	result = 1.0 - clamp(result, 0.0, 1.0);
	return result;
}

void main()
{
	float fogDist = abs(f_eye.z / f_eye.w);
	if (isDay)
		o_colour = texture(night, f_tex) * vec4(0.1, 0.1, 0.1, 1.0);
	else
		o_colour = texture(day, f_tex) * vec4(0.1, 0.1, 0.1, 1.0);

	if (f.type == 0 && fogDist > f.end) 
		o_colour = f.colour;
	else {
		float colDiff = time / 180.0f;
		if (time > 180.0f) {
			// Count backwards if it's past midday.
			float diffFloor = floor(colDiff);
			colDiff = diffFloor - mod(colDiff, diffFloor);
		}

		if (isDay)			
			o_colour = mix(o_colour, texture(night, f_tex), colDiff);
		else
			o_colour = mix(o_colour, texture(day, f_tex), colDiff);
	}

	o_colour = mix(o_colour, vec4(f.colour.rgb, 1.0), calcFogFactor(f, fogDist));
}