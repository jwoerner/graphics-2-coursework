#version 140

uniform mat4 view;
uniform mat4 projection;
uniform mat4 model;

in vec3 v_pos;

void main()
{
	gl_Position = projection * view * model * vec4(v_pos, 1.0);
}