#version 140

struct Fog {	
	vec4 colour;	// The colour of the fog.
	float start;	// The distance the fog begins.
	float end;		// The distacne the fog ends.
	float density;	// The density of the fog.
	int type;		// The equation for calculating fog density (0 = linear, 1 = exp, 2 = exp2)
};

in vec4 f_col;
in vec2 f_tex;
in float f_fogDist;

uniform	sampler2D diffuseTex;
uniform Fog f;

out vec4 o_colour;

void main() {
	
	if (f.type == 0 && f_fogDist > f.end) 
		discard; // Exit this frag calculation  early if we can.

	vec2 f_tex_f = vec2(f_tex.x, (1.0 - f_tex.y)); // Invert the UV y co-ord.

	o_colour = f_col;// * texture(diffuseTex, f_tex);
}