#version 140

uniform mat4 view;
uniform mat4 projection;
uniform mat4 model;
uniform mat3 normal;
uniform mat4 v_inv;

struct Light {
	vec4 pos;		// Position of the light.
	vec3 spotDir;	// Direction the spotlight is pointing.
	vec3 diffuse;	// Diffuse relfection of the light.
	vec3 specular;	// Specular reflection of the light.
	float spotSize;	// Size the spotlight emits full light to.
	float spotCutoff; // Size the spotlight emits zero light at.
	float spotMult;	// Power of the spotlight.
	bool enabled;	// Is the light enabled or not?
	/* Attenuation variables. */
	float constAttenuation, linearAttenuation, quadAttenuation;
};

struct Fog {	
	vec4 colour;	// The colour of the fog.
	float start;	// The distance the fog begins.
	float end;		// The distacne the fog ends.
	float density;	// The density of the fog.
	int type;		// The equation for calculating fog density (0 = linear, 1 = exp, 2 = exp2)
};

struct Material {
	vec4 ambient;
	vec4 diffuse;
	vec3 spec;
	float shine;
};

uniform	sampler2D ambientTex;
uniform sampler2D specularTex;

const int lightCount = 10;
uniform Light lights[lightCount];
uniform Material m;
uniform Fog f;

in vec3 v_pos;
in vec3 v_nor;
in vec2 v_tex;

out vec4 f_col;
out vec3 f_spec;
out vec2 f_tex;
out float f_fogDist;

/*
 Calculates the fog for this fragment.
 */
float calcFogFactor(Fog fog, float fogPos) {
	
	float result = 0.0;

	if (fog.type == 0) {
		// Linear fog.
		result = (f.end - fogPos)/(fog.end - fog.start);
	} else if (fog.type == 1) {
		// Exp fog.
		result = exp(-fog.density * fogPos);
	} else if (fog.type == 2) {
		// Exp2 fog.
		result = exp(-pow(fog.density * fogPos, 2.0));
	}

	result = 1.0 - clamp(result, 0.0, 1.0);
	return result;
}

void main()
{
	vec3 surfaceColour = vec3(0.0, 0.0, 0.0);
	float atten;

	vec3 lightDir = vec3(0, 0, 0);
	vec3 viewDir = normalize(vec3(v_inv * vec4(0.0, 0.0, 0.0, 1.0)) - v_pos);
	vec3 v_nor_n = normalize(normal * v_nor);
	vec4 v_eye = view * model * vec4(v_pos, 1.0);
	
	float fogDist = abs(v_eye.z / v_eye.w);

	for (int i = 0; i < lightCount; i++) {
		if (!lights[i].enabled) continue; // Skip this light if it's disabled.
		vec3 lightToVert = (lights[i].pos.xyz - f_pos);
		float distance = length(lightToVert);
		atten = mix(1.0, 
				1.0 / (lights[i].constAttenuation
					+ (lights[i].linearAttenuation * distance)
					+ (lights[i].quadAttenuation * distance * distance)),
				lights[i].pos.w);
		lightDir = normalize(lightToVert / distance);
	
		// Check if the light is a spotlight next.
		if (lights[i].spotSize <= 90.0) {
			// Spotlight
			float cosine = max(0.0, dot(-lightDir, lights[i].spotDir));
			if (cosine < cos(radians(lights[i].spotSize))) {
				// Fragment outside of spotlight influence.
				atten = 0.0;
			} else {
				atten = atten * pow(cosine, lights[i].spotMult);
			}
		}
	
		float diffIntensity = max(dot(v_nor_n, lightDir), 0.0);
		vec3 diffReflection = atten * lights[i].diffuse * diffIntensity;
		vec3 specReflection = vec3(0, 0, 0);
		if (dot(v_nor_n, lightDir) >= 0.0) {
			/* Calculate the specular if the angle of reflection is greater than zero. */
			specReflection = atten * lights[i].specular * m.spec * texture(specularTex, v_tex).a *
				pow(max(dot(reflect(-lightDir, v_nor_n), viewDir), 0.0), m.shine);
		}
		
		surfaceColour += diffReflection + specReflection;
	}

	f_tex = v_tex;
	f_col = vec4(surfaceColour, m.diffuse.a);
	f_fogDist = fogDist;

	f_col = mix(f_col, f.colour, calcFogFactor(f, fogDist));

	gl_Position = projection * v_eye;
}