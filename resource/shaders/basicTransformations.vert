#version 140

uniform mat4 view;
uniform mat4 projection;
uniform mat4 model;
uniform mat3 normal;

in vec3 v_pos;
in vec3 v_nor;
in vec2 v_tex;

out vec3 f_pos;
out vec3 f_nor;
out vec2 f_tex;
out vec4 f_eye;

void main()
{
	vec4 v_eye = view * model * vec4(v_pos, 1.0);

	f_pos = vec3(model * vec4(v_pos, 1.0));
	f_nor = normalize(normal * v_nor);
	f_tex = v_tex;
	f_eye = v_eye;

	gl_Position = projection * v_eye;
}