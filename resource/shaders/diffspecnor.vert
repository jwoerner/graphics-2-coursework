#version 140

uniform mat4 view;
uniform mat4 projection;
uniform mat4 model;
uniform mat3 normal;

in vec3 v_pos;
in vec3 v_nor;
in vec2 v_tex;

out vec3 f_pos;
out vec2 f_tex;
out vec4 f_eye;
out mat3 surfaceToWorld;

void main()
{
	f_pos = vec3(model * vec4(v_pos, 1.0));
	f_tex = vec2(v_tex.x, (1.0-v_tex.y));
	
	vec3 v_tan = cross(v_nor, vec3(0.0, 0.0, 1.0));
	if (length(v_tan) == 0) {
		v_tan = cross(v_nor, vec3(0.0, 1.0, 0.0));
	}
	v_tan = normalize(v_tan);
	surfaceToWorld[0] = normalize(vec3(model * vec4(v_tan, 0.0)));
	surfaceToWorld[2] = normalize(normal * v_nor);
	surfaceToWorld[1] = normalize(cross(surfaceToWorld[2], surfaceToWorld[1]));

	gl_Position = projection * view * model * vec4(v_pos, 1.0);
}